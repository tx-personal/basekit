/**
 * 
 */
package com.xracoon.util.basekit.system;

import com.xracoon.util.basekit.StringEx;

/**
 * @author 123
 *
 */
public class ExecStatus {
	private String log;
	private String error;
	private int retVal=Integer.MIN_VALUE;
	@Override
	public String toString(){
		return retVal==0?StringEx.emptyFix(log):StringEx.emptyFix(log)+"\r\nerror:\r\n"+StringEx.emptyFix(error);
	}
	
	public ExecStatus()
	{}
	public ExecStatus(String log, String error, int retVal){
		this.log=log;
		this.error=error;
		this.retVal=retVal;
	}
	
	public String getLog() {
		return log==null?"":log;
	}
	public void setLog(String log) {
		this.log = log;
	}
	public String getError() {
		return error==null?"":error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public int getRetVal() {
		return retVal;
	}
	public void setRetVal(int retVal) {
		this.retVal = retVal;
	}
}
