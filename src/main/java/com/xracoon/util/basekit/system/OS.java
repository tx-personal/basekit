package com.xracoon.util.basekit.system;

import static java.nio.file.attribute.PosixFilePermission.*;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.xracoon.util.basekit.FilesEx;
import com.xracoon.util.basekit.StringEx;
import com.xracoon.util.basekit.hudson.proc.Proc;

/**
 * OS抽象基类
 * @author Yangtianxin
 */
public abstract class OS implements Serializable {
	
	private static final long serialVersionUID = -2087192669943032904L;
	protected transient Logger logger = LoggerFactory.getLogger(OS.class);
	private String os;
	private String version;
	private String arch;
	private String name;
	private String domain;
	private String ip;
	private static final String defaultNetinterfacePattern="Intel|Realtek|TP-LINK|atheros";
	
	public boolean autoDeleteTempFile=false;
	public boolean tempFileInPwd=false;
	public abstract String[] getShellBin();
	public abstract String getHostPath();
	public abstract String getKillCmd(String pid);
	public abstract String getEnvSettingClause(String key, String value);
	
	private volatile static OS singleton;
	public static OS getSingleton() {
		if (singleton == null) {
			synchronized (OS.class) {
				if (singleton == null) {
					singleton = OSDetector.Detect();
				}
			}
		}
		return singleton;
	}
	
	public static OS getNewInstance(){
		return OSDetector.Detect();
	}
	
	
	public OS clone(){
		try {
			ByteArrayOutputStream buffer=new ByteArrayOutputStream();
			ObjectOutputStream oo=new ObjectOutputStream(buffer);
			oo.writeObject(this);
			ObjectInputStream oi=new ObjectInputStream(new ByteArrayInputStream(buffer.toByteArray()));
			OS cloneOS=(OS)oi.readObject();
			return cloneOS;
		} catch (Exception e) 
		{
			logger.error(e.getMessage(), e);
			return null;
		}
	}
	
	public void setLogger(Logger logger){
		this.logger=logger;
	}

	public static boolean isUnixLike(){
		return File.pathSeparatorChar==':';
	}
	public static boolean isMac(){
		return System.getProperty("os.name").toLowerCase().contains("mac os");
	}
	
	public String getLocalIP() throws UnknownHostException
	{
		  InetAddress ia = InetAddress.getLocalHost();  
	      return ia.getHostAddress();  
	}
	
	public String getIPByHost(String hostname) throws UnknownHostException
	{
		InetAddress[] addressArr = InetAddress.getAllByName(hostname);  
		return addressArr[0].getHostAddress();
	}
	
	/**
	 * get ip
	 * @param displayPattern  regex pattern of network interface's name; default value 'OS.defaultNetinterfacePattern' will be used if this param is null or empty
	 * @return ip
	 */
	public synchronized String getIP(String displayPattern)
	{
//		try {
//			logger.info("try get local IP: "+getLocalIP());
//		} catch (UnknownHostException e1) {
//			e1.printStackTrace();
//		}
		
		if(ip!=null)
			return ip;
		
		if(displayPattern==null || displayPattern.trim().length()==0)
		{
			logger.info("displayPattern is not set, use default: "+defaultNetinterfacePattern);
			displayPattern=defaultNetinterfacePattern;
		}
		
		Pattern pattern=Pattern.compile(displayPattern.toLowerCase());
		try{
			Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
			NetworkInterface networkInterface;
			Enumeration<InetAddress> inetAddresses;
			InetAddress inetAddress;
			//logger.info("====net dev pattern:  "+displayPattern);
			while (networkInterfaces.hasMoreElements()) {
			    networkInterface = networkInterfaces.nextElement();
			    if(networkInterface.isVirtual() || networkInterface.isLoopback() || networkInterface.isPointToPoint() || !networkInterface.isUp())
			    	continue;
			    
			    //logger.info("====net dev:  "+networkInterface.getDisplayName());
			    
			    Matcher matcher=pattern.matcher(networkInterface.getDisplayName().toLowerCase());
			    if(matcher.find())
			    {
			    	//logger.info("====net dev match:  "+networkInterface.getDisplayName());
				    inetAddresses = networkInterface.getInetAddresses();
				    while (inetAddresses.hasMoreElements()) {
				        inetAddress = inetAddresses.nextElement();
				        if (inetAddress != null && inetAddress instanceof Inet4Address) { // IPV4
				            ip = inetAddress.getHostAddress();
				        }
				    }
			    }
			}
			logger.info("getIP result: "+ip);
			return ip;
		}catch(Exception e)
		{
			return null;
		}
	}

	
	public synchronized File backupHost(String toFile) throws IOException{
		String path=getHostPath();
		File file=new File(path);
		File backfile=new File(toFile);
		if(file.exists()){
			FilesEx.copy(file, new File(toFile), true);
			logger.info("host backuped to "+backfile.getAbsolutePath());
			return backfile;
		}else{
			logger.info("host file not found!");
			return null;
		}

	}
	
	public synchronized void resotreHost(String fromFile) throws IOException{
		String path=getHostPath();
		File file=new File(path);
		File from=new File(fromFile);
		if(from.exists())
			FilesEx.copy(new File(fromFile), file, true);
	}

	public void checkHosts(Map<String,String> hostmap) throws Exception{
		Map<String,String> splitedHostMap=new LinkedHashMap<String, String>();
		for(Entry<String,String> entry: hostmap.entrySet()){
			String[] hosts=entry.getKey().split("\\s+");
			for(String h:hosts)
				if(h.trim().length()>0)
					splitedHostMap.put(h, entry.getValue());
		}
		for(Entry<String,String> entry: splitedHostMap.entrySet()){
			String host=entry.getKey();
			String ip=entry.getValue().trim();
			String actual=InetAddress.getByName(host).getHostAddress();
			if(!ip.equals(actual))
				throw new Exception("host verify exception: "+host+", expect: "+ip +", actual: "+actual);
		}
	}
	
	public synchronized void appendHost(Map<String,String> hostIpMap) throws IOException{
		String path=getHostPath();
		PrintWriter writer =null;
		try{
			writer= new PrintWriter(new OutputStreamWriter(new FileOutputStream(path,true)));
			writer.println("");
			writer.println("# appended host by teamcat agent at "+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			for(String hostNames: hostIpMap.keySet()){
				writer.println(hostIpMap.get(hostNames)+"\t"+hostNames);
			}
			writer.flush();
		}finally{
			if(writer!=null)
				writer.close();
		}
	}
	
	public synchronized void updateHost(Map<String,String> hostIPMap) throws IOException{
		Map<String,String> tempMaps=new HashMap<String,String>();
		for(String key:hostIPMap.keySet())
			tempMaps.put(key, hostIPMap.get(key).toLowerCase());
		
		//读取host文件
		String path=getHostPath();
		File file=new File(path);
		List<String> lines=FilesEx.readLines(file.toString());
		for(int idx=0; idx<lines.size(); idx++){
			String line=lines.get(idx).trim();
			if(!StringEx.isBlank(line) && !line.startsWith("#")){
				String[] parts=line.split("\\s+");
				String ip=parts[0];
				String hostname="";
				for(int i=1; i<parts.length; i++){
					if(parts[i].startsWith("#"))
						break;
					hostname+=parts[i]+" ";
				}
				hostname=hostname.trim().toLowerCase();
				
				//需要修改
				if(!StringEx.isBlank(hostname) && tempMaps.containsKey(hostname)){
					String newIp=tempMaps.get(hostname);
					if(StringEx.isBlank(newIp)){
						lines.remove(idx);
						tempMaps.remove(hostname);
					}
					else if(newIp.equals(ip))
						tempMaps.remove(hostname);
					else{
						lines.set(idx, tempMaps.get(hostname)+"\t"+hostname);
						tempMaps.remove(hostname);
					}
				}
			}
		}
		
		//需要添加
		for(String key: tempMaps.keySet())
			lines.add(tempMaps.get(key)+"\t"+key);
		
		//写回hosts文件
		FilesEx.writeFile(lines,file);
	}
	
	
	public String execSyn(String cmdline)
	{
		ExecStatus status=execSyn(cmdline,null,null);
		return status.getRetVal()==0?status.getLog():"";
	}
	
	/**
	 * invoke "jps -l" to get running java process list, compare process name with  parameter:<b>processRegex</b> 
	 * @param processRegex regex of process
	 * @return matched process pid
	 */
	public String[] getJavaPid(String processRegex)
	{
		List<String> list=new ArrayList<String>();
		try{
			Pattern pattern=Pattern.compile(processRegex);
			Process p =Runtime.getRuntime().exec("jps -l");
			String line = null;
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
		    while ((line = in.readLine()) != null) {
		        String [] javaProcess = line.split(" ");
		        if (javaProcess.length > 1 && pattern.matcher(javaProcess[1]).find()) 
		        	list.add(javaProcess[0].trim());
		    }
		    return list.toArray(new String[0]);
		}catch(Exception e)
		{
			logger.error("getJavaPid Error: "+e.getMessage(),e);
			return null;
		}
	}
	
	public ExecStatus execSyn(String cmdline,String[] env, File pwd)
	{
		String[] cmds=cmdline.split("\\s+");
		return execSyn(cmds,env, pwd);
	}
	
	//use nio, need jdk7+
	public ExecStatus execScriptSyn(String script, String[] env, File pwd) throws IOException{
		Path path=null;
		Path tempDir= (pwd==null||!tempFileInPwd)?Paths.get(System.getProperty("java.io.tmpdir")):pwd.toPath();
		script=script.replaceAll("\r", "");
		if(isUnixLike()){
			FileAttribute<Set<PosixFilePermission>> attrs=
					PosixFilePermissions.asFileAttribute(
							EnumSet.of(OWNER_READ, OWNER_WRITE, OWNER_EXECUTE));
			path = Files.createTempFile(tempDir, "osexec", ".sh", attrs);
		}
		else
			path = Files.createTempFile(tempDir,"osexec", ".bat");
		Files.write(path, script.getBytes());
		ExecStatus status=execScriptFileSyn(path.toFile(),env,pwd);
		if(autoDeleteTempFile)
			Files.deleteIfExists(path);
		return status;
	}
	
	public ExecStatus execScriptFileSyn(File scriptfile, String[] env, File pwd) throws IOException{
		List<String> cmds=new ArrayList<>(Arrays.asList(getShellBin()));
		cmds.add(scriptfile.getCanonicalPath());
		return execSyn(cmds.toArray(new String[0]),env,pwd);
	}
	
	public ExecStatus execSyn(String[] cmds,String[] env, File pwd){
		ExecStatus status=new ExecStatus();
		try{
			ByteArrayOutputStream o=new ByteArrayOutputStream();
			ByteArrayOutputStream e=new ByteArrayOutputStream();
			
			int ret=new Proc.LocalProc(cmds, env, System.in, o, e, pwd, logger, true).join();
			status.setRetVal(ret);
			status.setLog(o.toString());
			status.setError(e.toString());
		} catch (IOException | InterruptedException e) {
			logger.error(e.getMessage(),e);
			status.setError("Exception==>"+e.getMessage());
			status.setRetVal(-99);
		}
		return status;
	}
	
	public static class ExecArg{
		private String[] cmds;
		private String[] env;
		private File pwd;
		private boolean showCmd;
		private Logger logger;
		private OutputStream ostream;
		private OutputStream estream;
		private InputStream istream;
		
		public static ExecArg Build(){			
			ExecArg arg=new ExecArg();
			arg.cmds=new String[0];
			arg.env=null;
			arg.pwd=new File(".");
			arg.showCmd=true;
			
			return arg;
		}
		public static ExecArg Build(String[] cmds, File pwd){
			ExecArg arg=new ExecArg();
			arg.cmds=cmds;
			arg.env=null;
			arg.pwd=pwd;
			arg.showCmd=true;
			return arg;
		}
		
		public ExecArg Cmds(String[] cmds){
			this.cmds=cmds;
			return this;
		}
		
		public ExecArg Cmdline(String cmdline){
			this.cmds=StringEx.tokenize(cmdline).toArray(new String[0]);
			return this;
		}
		
		public ExecArg Pwd(File pwd){
			this.pwd=pwd;
			return this;
		}
		public ExecArg ShowCmd(boolean showCmd){
			this.showCmd=showCmd;
			return this;
		}
		public ExecArg Logger(Logger logger){
			this.logger=logger;
			return this;
		}
		public ExecArg Env(String[] env){
			this.env=env;
			return this;
		}
		public ExecArg OutputStream(OutputStream os){
			this.ostream=os;
			return this;
		}
		public ExecArg ErrorStream(OutputStream os){
			this.estream=os;
			return this;
		}
		public ExecArg InputStream(InputStream is){
			this.istream=is;
			return this;
		}
	}
	
	public ExecStatus execSyn(ExecArg execArg){
		ExecStatus status=new ExecStatus();
		try{
			OutputStream os=execArg.ostream;
			OutputStream es=execArg.estream;
			if(execArg.ostream==null)
				os=new ByteArrayOutputStream();
			if(execArg.ostream==null && execArg.estream==null)
				es=new ByteArrayOutputStream();

			int ret=new Proc.LocalProc(execArg.cmds, execArg.env, System.in, os, es, execArg.pwd, execArg.logger, execArg.showCmd).join();
			status.setRetVal(ret);
			if(execArg.ostream==null)
				status.setLog(os.toString());
			if(execArg.ostream==null && execArg.estream==null)
				status.setError(es.toString());
		} catch (IOException | InterruptedException e) {
			logger.error(e.getMessage(),e);
			status.setError("Exception==>"+e.getMessage());
			status.setRetVal(-99);
		}
		return status;
	}
	
	public ExecStatus execSyn(String[] cmds,String[] env, File pwd, boolean showCmd){
		ExecStatus status=new ExecStatus();
		try{
			ByteArrayOutputStream o=new ByteArrayOutputStream();
			ByteArrayOutputStream e=new ByteArrayOutputStream();
			
			int ret=new Proc.LocalProc(cmds, env, System.in, o, e, pwd, logger, showCmd).join();
			status.setRetVal(ret);
			status.setLog(o.toString());
			status.setError(e.toString());
		} catch (IOException | InterruptedException e) {
			logger.error(e.getMessage(),e);
			status.setError("Exception==>"+e.getMessage());
			status.setRetVal(-99);
		}
		return status;
	}
	
/*	public int execAsyn(String[] cmds, String[] env, File pwd, Logger logger) throws InterruptedException, IOException{
		LogOutputStream losStd=new LogOutputStream(logger, Level.INFO);
		LogOutputStream losErr=new LogOutputStream(logger, Level.ERROR);
		int ret=new Proc.LocalProc(cmds, env, System.in, null, null, pwd, logger).join();
		return ret;
	}*/
	
	public boolean killProcess(String pid)
	{
		try{
	        ExecStatus stats= this.execSyn(getKillCmd(pid), null, new File("."));
	        if(stats.getRetVal()!=0)
	        	logger.error(stats.getError());
	        
	        return stats.getRetVal()==0;
		}
		catch(Exception e)
		{
			logger.error("killProcess Error: ", e);
			return false;
		}
	}
	
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getArch() {
		return arch;
	}
	public void setArch(String arch) {
		this.arch = arch;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
}

class OSDetector {
	static Logger logger = LoggerFactory.getLogger(OSDetector.class);
	
	static OS Detect(){
		 Properties props=System.getProperties();   
		 String os=props.getProperty("os.name"); 
		 String arch=props.getProperty("os.arch");
		 String version=props.getProperty("os.version");  

		 OS node=null;
		 if(os.toLowerCase().contains("windows")) //Windows 7
			 node=new OSWin();
		 else if(os.toLowerCase().contains("mac os")) //Mac OS X
			 node=new OSMac();
		 else if(File.pathSeparator.equals(":")) //linux
			 node =new OSLinux();
		 else
			 return null;
		 
		 node.setOs(os);
		 node.setArch(arch);
		 node.setVersion(version);
		 
		 Map<String,String> map = System.getenv();
	     node.setName(map.get("COMPUTERNAME"));
	     node.setDomain(map.get("USERDOMAIN"));
	     
		 return node;
	}
}
