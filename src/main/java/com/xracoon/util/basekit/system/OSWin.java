package com.xracoon.util.basekit.system;

/**
 * Windows系统类
 * @author Yangtianxin
 */
public class OSWin  extends OS{
	private static final long serialVersionUID = 5836943377817677487L;

	@Override
	public String[] getShellBin()
	{
		return new String[]{"cmd.exe","/c"};
	};
	
	@Override
	public String getHostPath()
	{
		return System.getenv("SystemRoot")+"/System32/drivers/etc/hosts";
	}

	@Override
	public String getKillCmd(String pid) {
		return "taskkill /F /T /PID "+pid;
	}

	@Override
	public String getEnvSettingClause(String key, String value) {
		return "SET "+key+"="+value;
	}
	
}
