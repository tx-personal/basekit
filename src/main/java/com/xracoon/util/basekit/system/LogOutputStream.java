package com.xracoon.util.basekit.system;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

import com.xracoon.util.basekit.StringEx;

//byte output -> buffer -> logger output
public class LogOutputStream extends OutputStream {
	private Logger logger=LoggerFactory.getLogger(LogOutputStream.class);
	private Level level=Level.INFO;
	private int maxBufferSize=1024*1024; //1M
	private OutputStream os=null;
	
	private Charset charset=Charset.defaultCharset();
	public LogOutputStream(Logger logger, Level level){
		this.logger=logger;
		this.level=level;
	}
	public LogOutputStream(OutputStream os, Logger logger, Level level){
		this.logger=logger;
		this.level=level;
		this.os=os;
	}
	public LogOutputStream(Logger logger, Level level, Charset charset){
		this.logger=logger;
		this.level=level;
		this.charset=charset;
	}
	
	public LogOutputStream(OutputStream os, Logger logger, Level level, Charset charset){
		this.logger=logger;
		this.level=level;
		this.charset=charset;
		this.os=os;
	}
	
	private ByteArrayOutputStream bufferBytes=new ByteArrayOutputStream();
	
	@Override
	public synchronized void write(int b) throws IOException {
		if(os!=null)
			os.write(b);
		bufferBytes.write(b);
		if(b=='\n' || bufferBytes.size()==maxBufferSize){
			writeLine();
		}
	}
	
	private void writeLine() throws UnsupportedEncodingException{
		String line=bufferBytes.toString(charset.name()).replaceAll("\\s+$", "");
		bufferBytes.reset();
		if(StringEx.isBlank(line))
			return;
		if(level==Level.ERROR)
			logger.error(line);
		else
			logger.info(line);
	}
	
    public synchronized void write(byte b[]) throws IOException {
        write(b, 0, b.length);
    }

    public synchronized void write(byte b[], int off, int len) throws IOException {
        if (b == null) {
            throw new NullPointerException();
        } else if ((off < 0) || (off > b.length) || (len < 0) ||
                   ((off + len) > b.length) || ((off + len) < 0)) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return;
        }
        for (int i = 0 ; i < len ; i++) {
            write(b[off + i]);
        }
    }

    public synchronized void flush() throws IOException {
    	writeLine();
    }

    public void close() throws IOException {
    	writeLine();
    }
}
