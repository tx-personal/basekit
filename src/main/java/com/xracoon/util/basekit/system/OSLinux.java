package com.xracoon.util.basekit.system;

/**
 * Windows系统类
 * @author Yangtianxin
 */
public class OSLinux  extends OS{
	private static final long serialVersionUID = 4329347439527017592L;

	@Override
	public String[] getShellBin(){
		return new String[]{"/bin/sh","-cxe"};
	}

	@Override
	public String getHostPath() {
		return "/etc/hosts";
	}

	@Override
	public String getKillCmd(String pid) {
		return "kill -9" +pid;
	}

	@Override
	public String getEnvSettingClause(String key, String value) {
		return "export "+key+"="+value;
	}
}
