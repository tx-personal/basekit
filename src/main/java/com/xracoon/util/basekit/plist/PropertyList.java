/**
* @author yangtianxin
* @date 2015-10-15
* Copyright 2015, .... All right reserved. 
*/

package com.xracoon.util.basekit.plist;

import java.util.Map;
import java.util.Map.Entry;


public class PropertyList {
	public final static String TPLIST="plist";
	public final static String AVERSION="version";
	public final static String TKEY="key";
	
	public final static String TVDict="dict";
	public final static String TVArray="array";
	
	public final static String TVInteger="integer";
	public final static String TVReal="real";
	public final static String TVString="string";
	public final static String TVDate="date";
	public final static String TVData="data";
	
	public final static String TVTrue="true";
	public final static String TVFalse="false";
	
	private String version;
	private String doctype;
	private String publicId;
	private String systemId;
	
	private String xmlEncode;
	private String xmlVersion;
	
	private PropData rootItem;
	
	
	public PropData getData(String key){
		return getData(key,rootItem);
	}
	
	private PropData getData(String key, PropData current){
		if(current==null)
			return null;
		
		PropData data=null;
		if(PropData[].class==current.getType()){
			for(PropData pData: (PropData[])current.getValue()){
				data=getData(key, pData);
				if(data!=null)
					break;
			}
		}
		else if(current.getValue() instanceof Map){
			@SuppressWarnings("unchecked")
			Map<String, PropData> map=(Map<String, PropData>) current.getValue();
			for(Entry<String,PropData> entry: map.entrySet()){
				if(entry.getKey().equalsIgnoreCase(key)){
					data=entry.getValue();
					break;
				}
				else{
					data= getData(key,entry.getValue());
					if(data!=null)
						break;
				}
			}
		}
		return data;
	}
	
	public void setRootItem(PropData item){
		rootItem=item;
	}
	public PropData getRootItem(){
		return rootItem;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getDoctype() {
		return doctype;
	}
	public void setDoctype(String doctype) {
		this.doctype = doctype;
	}
	public String getPublicId() {
		return publicId;
	}
	public void setPublicId(String publicId) {
		this.publicId = publicId;
	}
	public String getSystemId() {
		return systemId;
	}
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	public String getXmlEncode() {
		return xmlEncode;
	}
	public void setXmlEncode(String xmlEncode) {
		this.xmlEncode = xmlEncode;
	}
	public String getXmlVersion() {
		return xmlVersion;
	}
	public void setXmlVersion(String xmlVersion) {
		this.xmlVersion = xmlVersion;
	}
	

}
