/**
* @author yangtianxin
* @date 2015-10-15
* Copyright 2015, .... All right reserved. 
*/

package com.xracoon.util.basekit.plist;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import com.xracoon.util.basekit.StreamsEx;
import com.xracoon.util.basekit.XmlEx;

public class PlistUtil {
	private static SimpleDateFormat dateFormat;
	private static String localdtd;
	static{
		InputStream dtdIs=null;
		if((dtdIs=PlistUtil.class.getClassLoader().getResourceAsStream("DTDs/PropertyList-1.0.dtd"))!=null){
			try {
				File tfile = new File(System.getProperty("java.io.tmpdir"),"PropertyList-1.0.dtd");
				
				if(tfile!=null && tfile.exists()){
					StreamsEx.copy(dtdIs, new FileOutputStream(tfile), true);
					localdtd=tfile.getAbsolutePath();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public PropertyList parse(String content) throws ParserConfigurationException,
			SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setIgnoringElementContentWhitespace(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		if(localdtd!=null)
			content=content.replace("http://www.apple.com/DTDs/PropertyList-1.0.dtd", localdtd);
		
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(content));
		Document doc = builder.parse(is);

		Element xplist = doc.getDocumentElement(); 
		Element xroot = XmlEx.getFirstChild(xplist, Element.class);
		
		String rootTag=null;
		if(xroot!=null)
			rootTag=xroot.getTagName();
		
		if (xplist == null || !xplist.getTagName().equalsIgnoreCase(PropertyList.TPLIST) || xroot == null	
				|| !rootTag.equalsIgnoreCase(PropertyList.TVDict) && !rootTag.equalsIgnoreCase(PropertyList.TVArray) 
				|| XmlEx.getNextSibling(xroot,Element.class) != null)
			return null;

		PropertyList plist = new PropertyList();
		plist.setXmlEncode(doc.getXmlEncoding());
		plist.setXmlVersion(doc.getXmlVersion());
		
		if (doc.getDoctype() != null) {
			plist.setDoctype(doc.getDoctype().getName());
			plist.setPublicId(doc.getDoctype().getPublicId());
			plist.setSystemId(doc.getDoctype().getSystemId());
		}
		
		plist.setVersion(xplist.getAttribute(PropertyList.AVERSION));

		PropData rootItem=null;
		if(rootTag.equalsIgnoreCase(PropertyList.TVArray))
			rootItem=new PropData(parseArray(xroot));
		else
			rootItem=new PropData(parseDict(xroot));
		
		plist.setRootItem(rootItem);

		return plist;
	}
	
	private PropData[] parseArray(Element xelem){
		List<PropData> datalist=new ArrayList<PropData>();
		Element xvalue=XmlEx.getFirstChild(xelem, Element.class);
		while( xvalue!=null){
			String valueTag=xvalue.getTagName();
			
			if(valueTag.equalsIgnoreCase(PropertyList.TVArray)){
				PropData[] list=parseArray(xvalue);
				PropData item=new PropData(list);
				datalist.add(item);
			}
			else if(valueTag.equalsIgnoreCase(PropertyList.TVDict)){
				Map<String,PropData> map=parseDict(xvalue);
				PropData item=new PropData(map);
				datalist.add(item);
			}
			else{
				PropData data=parseSimpleType(xvalue);
				datalist.add(data);
			}
			
			xvalue=XmlEx.getFirstChild(xvalue, Element.class);
		}
		
		return datalist.toArray(new PropData[0]);
	}
	
	private Map<String,PropData> parseDict(Element xelem) {
		Element xkey= XmlEx.getFirstChild(xelem, Element.class);
		Element xvalue=XmlEx.getNextSibling(xkey, Element.class);
		
		Map<String,PropData> dict=new LinkedHashMap<String,PropData>();
		
		while(xkey!=null && xkey.getTagName().equalsIgnoreCase(PropertyList.TKEY) 
				&& xvalue!=null && !xvalue.getTagName().equalsIgnoreCase(PropertyList.TKEY)){
			String key=xkey.getTextContent();
			
//			System.out.println(key);
//			System.out.println("\t"+xvalue.getNodeName()+": "+value);
			
			String valueTag=xvalue.getTagName();
			PropData data=null;
			
			if(valueTag.equalsIgnoreCase(PropertyList.TVArray)){
				PropData[] list=parseArray(xvalue);
				data=new PropData(list);
			}
			else if(valueTag.equalsIgnoreCase(PropertyList.TVDict)){
				Map<String,PropData> map=parseDict(xvalue);
				data=new PropData(map);
			}
			else{
				data=parseSimpleType(xvalue);
			}
			dict.put(key, data);
			
			xkey=XmlEx.getNextSibling(xvalue, Element.class);
			if(xkey!=null)
				xvalue=XmlEx.getNextSibling(xkey, Element.class);
		}
		return dict;
	}
	
	private PropData parseSimpleType(Element xvalue){
		PropData data=null;
		String valueTag=xvalue.getTagName();
		String value=xvalue.getTextContent();
		
		if(valueTag.equalsIgnoreCase(PropertyList.TVString))
			 data=new PropData(value);

		else if(valueTag.equalsIgnoreCase(PropertyList.TVInteger))
			data=new PropData(Integer.parseInt(value));

		else if(valueTag.equalsIgnoreCase(PropertyList.TVReal))
			data=new PropData(Double.parseDouble(value));

		else if(valueTag.equalsIgnoreCase(PropertyList.TVDate)){
			try {
				Date date = getFormat().parse(value);
				//String str=getFormat().format(date);
				data=new PropData(date);
			} catch (ParseException e) {
			}
		}
		else if(valueTag.equalsIgnoreCase(PropertyList.TVData))
			data=new PropData(value.getBytes());

		else if(valueTag.equalsIgnoreCase(PropertyList.TVTrue) || valueTag.equalsIgnoreCase(PropertyList.TVFalse))
			data=new PropData(Boolean.parseBoolean(valueTag));
		
		return data;
	}
	
	private SimpleDateFormat getFormat(){
		if(dateFormat==null){
			dateFormat=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");// 2015-01-05T03:31:58Z
		}
		return dateFormat;
	}
	
	
	public Document format(PropertyList plist) throws ParserConfigurationException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setIgnoringElementContentWhitespace(true);
		DocumentBuilder builder = factory.newDocumentBuilder();

		Document doc=builder.newDocument();
		doc.setXmlVersion(plist.getXmlVersion());
		Element xplist=doc.createElement(PropertyList.TPLIST);
		xplist.setAttribute(PropertyList.AVERSION, plist.getVersion());
		doc.appendChild(xplist);
		
		PropData dataItem=plist.getRootItem();
		Element xroot=null;
		if(dataItem.getValue() instanceof Map){
			@SuppressWarnings("unchecked")
			Map<String,PropData> map=(Map<String, PropData>) dataItem.getValue();
			xroot=formatDict(map,doc);
		}
		else if(dataItem.getType()==PropData[].class){
			PropData[] arrays=(PropData[]) dataItem.getValue();
			xroot=formatArray(arrays,doc);
		}
		
		xplist.appendChild(xroot);
		return doc;
	}
	

	private Element formatDict(Map<String, PropData> map, Document doc){
		Element xelem=doc.createElement(PropertyList.TVDict);
		for(Entry<String, PropData> entry:map.entrySet()){
			Element xkey=doc.createElement(PropertyList.TKEY);
			xkey.setTextContent(entry.getKey());
			xelem.appendChild(xkey);
			
			PropData pData=map.get(entry.getKey());
			Element xvalue=null;
			if(pData.getValue() instanceof Map){
				@SuppressWarnings("unchecked")
				Map<String,PropData> data=(Map<String, PropData>) pData.getValue();
				xvalue=formatDict(data,doc);
			}
			else if(pData.getType()==PropData[].class){
				PropData[] arrays=(PropData[]) pData.getValue();
				xvalue=formatArray(arrays,doc);
			}
			else{
				xvalue =formatSimpleType(pData, doc);
			}
			xelem.appendChild(xvalue);
		}
		
		return xelem;
	}
	

	private Element formatArray(PropData[] array, Document doc){
		Element xelem=doc.createElement(PropertyList.TVArray);
		for(PropData pData: array){
			Element xvalue=null;
			if(pData.getType()==TreeMap.class){
				@SuppressWarnings("unchecked")
				Map<String,PropData> data=(Map<String, PropData>) pData.getValue();
				xvalue=formatDict(data,doc);
			}
			else if(pData.getType()==PropData[].class){
				PropData[] arrays=(PropData[]) pData.getValue();
				xvalue=formatArray(arrays,doc);
			}
			else{
				xvalue =formatSimpleType(pData, doc);
			}
			xelem.appendChild(xvalue);
		}
		return xelem;
	}
	
	private Element formatSimpleType(PropData pData, Document doc){
		Element xvalue=null;
		if(pData.getType()==String.class){
			xvalue=doc.createElement(PropertyList.TVString);
			xvalue.setTextContent(pData.getValue().toString());
		}
		else if(pData.getType()==Double.class){
			xvalue=doc.createElement(PropertyList.TVReal);
			xvalue.setTextContent(pData.getValue().toString());
		}
		else if(pData.getType()==Integer.class){
			xvalue=doc.createElement(PropertyList.TVInteger);
			xvalue.setTextContent(pData.getValue().toString());
		}
		else if(pData.getType()==Boolean.class){
			Boolean data=(Boolean) pData.getValue();
			if(data!=null && data)
				xvalue=doc.createElement(PropertyList.TVTrue);
			else
				xvalue=doc.createElement(PropertyList.TVFalse);
		}
		else if(pData.getType()==byte[].class){
			xvalue=doc.createElement(PropertyList.TVData);
			xvalue.setTextContent(new String((byte[])pData.getValue()));
		}
		else if(pData.getType()==Date.class){		
			xvalue=doc.createElement(PropertyList.TVDate);
			String dateStr=getFormat().format((Date)pData.getValue());//.replace("UTC", "Z");
			xvalue.setTextContent(dateStr);
		}
		return xvalue;
	}
}
