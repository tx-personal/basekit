package com.xracoon.util.basekit.plist;
/**
* @author yangtianxin
* @date 2015-10-15
* Copyright 2015, .... All right reserved. 
*/

import java.lang.reflect.Array;

public class PropData {
	private Object value;
	
	public PropData(Object value){
		this.value=value;
	}
	
	public Object[] getArray(Class<?> cls){
		PropData[] da=null;
		if(value.getClass().isArray() && (da=(PropData[])value)!=null ){
			if(da.length>0 && !cls.isAssignableFrom(da[0].getType()) && cls.equals(da[0].getType()))
				return null;
				
			Object array = Array.newInstance(cls, da.length);
			for(int i=0; i<da.length; i++)
				Array.set(array, i, da[i].getValue());
			return (Object[]) array;
		}
		return null;
	}
	
	public Object getValue() {
		return value;
	}
	
	public Class<?> getType(){
		return value==null?null:value.getClass();
	}
}
