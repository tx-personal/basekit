package com.xracoon.util.basekit;

import java.io.File;
import java.nio.file.Path;

/**
 * PathEx作为封装的类对象使用，FilesEx作为工具类使用
 */
public class PathEx {
	private File file;
	
	public PathEx(File file){
		this.file=file;
	}
	public PathEx(Path path){
		this.file=path.toFile();
	}
	
}
