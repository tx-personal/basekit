package com.xracoon.util.basekit;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Node;

public class XmlEx {
	@SuppressWarnings("unchecked")
	public static <T> T getFirstChild(Node node,Class<T> c){
		for(Node n= node.getFirstChild(); n!=null; n=n.getNextSibling())
			if(c.isAssignableFrom(n.getClass()))
				return (T)n;
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getLastChild(Node node,Class<T> c){
		for(Node n= node.getLastChild(); n!=null; n=n.getPreviousSibling())
			if(c.isAssignableFrom(n.getClass()))
				return (T)n;
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getNextSibling(Node node,Class<T> c){
		for(Node n= node.getNextSibling(); n!=null; n=n.getNextSibling())
			if(c.isAssignableFrom(n.getClass()))
				return (T)n;
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getPreviousSibling(Node node,Class<T> c){
		for(Node n= node.getPreviousSibling(); n!=null; n=n.getPreviousSibling())
			if(c.isAssignableFrom(n.getClass()))
				return (T)n;
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> List<T> getChildern(Node node,Class<T> c){
		List<T> list=new ArrayList<>();
		for(Node n= node.getFirstChild(); n!=null; n=n.getNextSibling())
			if(c.isAssignableFrom(n.getClass()))
				list.add((T)n);
		return list;
	}
}
