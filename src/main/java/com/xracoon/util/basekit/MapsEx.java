package com.xracoon.util.basekit;

import java.util.Map;

public class MapsEx {
    public static <T> T getValue(Map<Object, Object> map, Object key, Class<T> klass) {
        Object obj = map.get(key);
        if (obj == null)
            return null;
        return klass.cast(obj);
    }
}
