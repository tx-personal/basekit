/**
* @author yangtianxin
* @date 2015-10-15
* Copyright 2015, .... All right reserved. 
*/

package com.xracoon.util.basekit.ssh;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Identity;
import com.jcraft.jsch.IdentityRepository;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.KeyPair;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.xracoon.util.basekit.StringEx;
import com.xracoon.util.basekit.security.auth.Credential;
import com.xracoon.util.basekit.system.ExecStatus;
import com.xracoon.util.basekit.system.LogOutputStream;

public class JschSSHUtil extends SSHUtil{	
	private Logger logger = LoggerFactory.getLogger(JschSSHUtil.class);
	
	private String host;
	private int port=22;
	private Credential cred;
	private JSch jsch=new JSch();
	
	public JschSSHUtil(String host, int port, String user, char[] passwd){
		this.host=host;
		this.port=port;
		cred=Credential.Plain(user, passwd);
	}
	/**
	 * use pemKey of Credential
	 * 
	 * @param host host
	 * @param port port
	 * @param cred cred
	 * @throws IOException exception
	 * @throws GeneralSecurityException exception
	 * @throws JSchException exception
	 */
	public JschSSHUtil(String host, int port, Credential cred) throws IOException, GeneralSecurityException, JSchException{
		this.host=host;
		this.port=port;
		this.cred=cred;
		if(!StringEx.isBlank(cred.getPemKey()))
			jsch.addIdentity(IdentityFileEx.newInstance("keypair_of_"+cred.getUser(), cred.getPemKey().getBytes(), null, jsch)
					, new String(cred.getPasswd()).getBytes());
	}
	
	/*
	 * if path ends with '/' full path will create, else create parent path. 
	 */
	public void mkdirs(ChannelSftp sftp, String path) throws IOException, SftpException{
		path=path.replace('\\', '/');
		List<String> pathSegment=new ArrayList<String>();
		Path parent=Paths.get(path);
		if(!path.endsWith("/") && !path.endsWith("\\"))
			parent=parent.getParent();
		
		Path current=parent;
		while(current!=null){
			try{   
				sftp.stat(current.toString().replace('\\', '/'));
				break;
			}
			catch(Exception e){
				if(e.getMessage().equals("No such file") && current.getFileName()!=null 
						&& !current.getFileName().toString().equals("~")
						&& !current.getFileName().toString().equals("."))
					pathSegment.add(current.toString().replace('\\', '/'));
				current=current.getParent();
			}
		}
		
		for(int i=pathSegment.size()-1; i>=0; i--)
			sftp.mkdir(pathSegment.get(i));
		
		sftp.stat(parent.toString().replace('\\', '/'));
	}
	
	@Override
	public void writeFile(InputStream is, String remote) throws IOException, JSchException, SftpException, InterruptedException{
		Session session=null; 
		ChannelSftp sftp=null;
		try{
			remote=remote.replace("~", ".");
			session=jsch.getSession(cred.getUser(), host, port);
			session.setConfig("StrictHostKeyChecking", "no");
			if(StringEx.isBlank(cred.getPemKey()))
				session.setPassword(new String(cred.getPasswd()));
			session.connect();
			sftp=(ChannelSftp)session.openChannel("sftp");
			sftp.connect();
			mkdirs(sftp,remote);
			sftp.put(is, remote);
			
			logger.info("---write done---");
		}finally{
			if(sftp!=null)
				sftp.disconnect();
			if(session!=null)
				session.disconnect();
		}
	}
	
	@Override
	public void readFile(String remote, OutputStream os) throws IOException, JSchException, SftpException{
		Session session=null; 
		ChannelSftp sftp=null;
		try{
			session=jsch.getSession(cred.getUser(), host, port);
			session.setConfig("StrictHostKeyChecking", "no");
			if(StringEx.isBlank(cred.getPemKey()))
				session.setPassword(new String(cred.getPasswd()));
			session.connect();
			
			sftp=(ChannelSftp)session.openChannel("sftp");
			sftp.connect();
			if(os == null)
				os = new ByteArrayOutputStream();
			sftp.get(remote, os);
			logger.info("---read done---");
		}finally
		{
			if(sftp!=null)
				sftp.disconnect();
			if(session!=null)
				session.disconnect();
		}
	}
	
	public Session openSession() throws JSchException{
		Session session=null; 
		session=jsch.getSession(cred.getUser(), host, port);
		session.setConfig("StrictHostKeyChecking", "no");
		if(StringEx.isBlank(cred.getPemKey()))
			session.setPassword(new String(cred.getPasswd()));
		session.connect();
		return session;
	}

	public void closeSession(Object sessionObj) {
		Session session=(Session)sessionObj;
		session.disconnect();
	}

	public ExecStatus remoteExecute(Object sessionObj, String cmdline) throws Exception{
		Session session=(Session)sessionObj;
		ChannelExec execChannel=null;
		try{
			execChannel=(ChannelExec)session.openChannel("exec");
			execChannel.setCommand(cmdline);
			ByteArrayOutputStream os=new ByteArrayOutputStream();
			ByteArrayOutputStream es=new ByteArrayOutputStream();
			execChannel.setInputStream(null);
			execChannel.setOutputStream(new LogOutputStream(os, logger, Level.INFO));
			execChannel.setErrStream(new LogOutputStream(es, logger, Level.ERROR));
			execChannel.connect();
			
			while(!execChannel.isClosed())
				Thread.sleep(500);
		
			logger.info("---exec done: "+execChannel.getExitStatus()+"---");
			ExecStatus execStatus=new ExecStatus(os.toString(),es.toString(),execChannel.getExitStatus());
			return execStatus;
		}finally{
			if(execChannel!=null)
				execChannel.disconnect();
		}
	}
	
	@Override
	public ExecStatus remoteExecute(String cmdline) throws IOException, JSchException, InterruptedException{
		logger.info("---exec command over ssh---");
		logger.info(cmdline);
		logger.info("---------------------------");
		Session session=null; 
		ChannelExec execChannel=null;
		try{
			session=jsch.getSession(cred.getUser(), host, port);
			session.setConfig("StrictHostKeyChecking", "no");
			if(StringEx.isBlank(cred.getPemKey()))
				session.setPassword(new String(cred.getPasswd()));
			session.connect();
			execChannel=(ChannelExec)session.openChannel("exec");
			execChannel.setCommand(cmdline);
			ByteArrayOutputStream os=new ByteArrayOutputStream();
			ByteArrayOutputStream es=new ByteArrayOutputStream();
			execChannel.setInputStream(null);
			execChannel.setOutputStream(new LogOutputStream(os, logger, Level.INFO));
			execChannel.setErrStream(new LogOutputStream(es, logger, Level.ERROR));
			execChannel.connect();
			
			while(!execChannel.isClosed())
				Thread.sleep(500);
		
			logger.info("---exec done: "+execChannel.getExitStatus()+"---");
			ExecStatus execStatus=new ExecStatus(os.toString(),es.toString(),execChannel.getExitStatus());
			return execStatus;
		}finally{
			if(execChannel!=null)
				execChannel.disconnect();
			if(session!=null)
				session.disconnect();
		}
	}
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public Credential getCred() {
		return cred;
	}


	/* -*-mode:java; c-basic-offset:2; indent-tabs-mode:nil -*- */
	/*
	Copyright (c) 2002-2015 ymnk, JCraft,Inc. All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	  1. Redistributions of source code must retain the above copyright notice,
	     this list of conditions and the following disclaimer.

	  2. Redistributions in binary form must reproduce the above copyright 
	     notice, this list of conditions and the following disclaimer in 
	     the documentation and/or other materials provided with the distribution.

	  3. The names of the authors may not be used to endorse or promote products
	     derived from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL JCRAFT,
	INC. OR ANY CONTRIBUTORS TO THIS SOFTWARE BE LIABLE FOR ANY DIRECT, INDIRECT,
	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
	LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
	OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
	EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*/
	public static class IdentityFileEx implements Identity{
	  private KeyPair kpair;
	  private String identity;

	  static IdentityFileEx newInstance(String prvfile, String pubfile, JSch jsch) throws JSchException{
	    KeyPair kpair = KeyPair.load(jsch, prvfile, pubfile);
	    return new IdentityFileEx(prvfile, kpair);
	  }

	  static IdentityFileEx newInstance(String name, byte[] prvkey, byte[] pubkey, JSch jsch) throws JSchException{
	    KeyPair kpair = KeyPair.load(jsch, prvkey, pubkey);
	    return new IdentityFileEx(name, kpair);
	  }

	  private IdentityFileEx(String name, KeyPair kpair) throws JSchException{
	    this.identity = name;
	    this.kpair = kpair;
	  }

	  /**
	   * Decrypts this identity with the specified pass-phrase.
	   * @param passphrase the pass-phrase for this identity.
	   * @return <tt>true</tt> if the decryption is succeeded
	   * or this identity is not cyphered.
	   */
	  public boolean setPassphrase(byte[] passphrase) throws JSchException{
	    return kpair.decrypt(passphrase);
	  }

	  /**
	   * Returns the public-key blob.
	   * @return the public-key blob
	   */
	  public byte[] getPublicKeyBlob(){
	    return kpair.getPublicKeyBlob();
	  }

	  /**
	   * Signs on data with this identity, and returns the result.
	   * @param data data to be signed
	   * @return the signature
	   */
	  public byte[] getSignature(byte[] data){
	    return kpair.getSignature(data);
	  }

	  /**
	   * @deprecated This method should not be invoked.
	   * @see #setPassphrase(byte[] passphrase)
	   */
	  public boolean decrypt(){
	    throw new RuntimeException("not implemented");
	  }

	  /**
	   * Returns the name of the key algorithm.
	   * @return "ssh-rsa" or "ssh-dss"
	   */
	  public String getAlgName(){
		switch(kpair.getKeyType()){
			case KeyPair.RSA: return "ssh-rsa";
			case KeyPair.DSA: return "ssh-dss";
			case KeyPair.ECDSA: return "ssh-ecdsa";
			default: return "unknown";
		}
	  }

	  /**
	   * Returns the name of this identity. 
	   * It will be useful to identify this object in the {@link IdentityRepository}.
	   */
	  public String getName(){
	    return identity;
	  }

	  /**
	   * Returns <tt>true</tt> if this identity is cyphered.
	   * @return <tt>true</tt> if this identity is cyphered.
	   */
	  public boolean isEncrypted(){
	    return kpair.isEncrypted();
	  }

	  /**
	   * Disposes internally allocated data, like byte array for the private key.
	   */
	  public void clear(){
	    kpair.dispose();
	    kpair = null;
	  }

	  /**
	   * Returns an instance of {@link KeyPair} used in this {@link Identity}.
	   * @return an instance of {@link KeyPair} used in this {@link Identity}.
	   */
	  public KeyPair getKeyPair(){
	    return kpair;
	  }
	}
}
