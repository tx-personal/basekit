/**
* @author yangtianxin
* @date 2015-10-15
* Copyright 2015, .... All right reserved. 
*/

package com.xracoon.util.basekit.ssh;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;
import org.apache.sshd.client.SshClient;
import org.apache.sshd.client.channel.ChannelExec;
import org.apache.sshd.client.channel.ClientChannel;
import org.apache.sshd.client.future.AuthFuture;
import org.apache.sshd.client.future.ConnectFuture;
import org.apache.sshd.client.session.ClientSession;
import org.apache.sshd.client.subsystem.sftp.SftpClient;
import org.apache.sshd.client.subsystem.sftp.SftpClient.OpenMode;
import org.apache.sshd.common.config.keys.FilePasswordProvider;
import org.apache.sshd.common.util.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import com.xracoon.util.basekit.StreamsEx;
import com.xracoon.util.basekit.security.auth.Credential;
import com.xracoon.util.basekit.system.ExecStatus;
import com.xracoon.util.basekit.system.LogOutputStream;

@Deprecated
class SshdSSHUtil extends SSHUtil{	
	private Logger logger = LoggerFactory.getLogger(SshdSSHUtil.class);
	private String host;
	private int port=22;
	private String remoteUser;
	private KeyPair privKey;
	private String password;
	private int maxCmdTimeout=600000;
	
	public SshdSSHUtil(String remoteUser, String host, int port,  String privKeyFile) throws IOException, GeneralSecurityException
	{
		this.host=host;
		this.port=port;
		this.remoteUser=remoteUser;
		this.privKey=SecurityUtils.loadKeyPairIdentity("privKeyIdentity", new FileInputStream(privKeyFile), null);
	}
	
	public SshdSSHUtil(String remoteUser, String host, int port,  InputStream privKey) throws IOException, GeneralSecurityException
	{
		this.host=host;
		this.port=port;
		this.remoteUser=remoteUser;
		this.privKey=SecurityUtils.loadKeyPairIdentity("privKeyIdentity", privKey, null);
	}
	
	public SshdSSHUtil(String host, int port, final Credential cred) throws IOException, GeneralSecurityException
	{
		this.host=host;
		this.port=port;
		this.remoteUser=cred.getUser();
		this.password=new String(cred.getPasswd());
		if(cred.isSecretKey()){
			this.privKey=SecurityUtils.loadKeyPairIdentity("privKeyIdentity", 
					new ByteArrayInputStream(cred.getPemKey().getBytes()), 
					(cred.getPasswd()==null||cred.getPasswd().length==0)?null: 
						new FilePasswordProvider() {
	                        @Override
	                        public String getPassword(String file) throws IOException {
	                        	return new String(cred.getPasswd());
	                        }
	                    }
			);
		}
	}
	
	public SshdSSHUtil(String host, int port, String remoteUser, char[] password) throws IOException, GeneralSecurityException
	{
		this.host=host;
		this.port=port;
		this.remoteUser=remoteUser;
		this.password=new String(password);
	}
	
	public void setMaxCmdTimeout(int timeout)
	{
		this.maxCmdTimeout=timeout;
	}
	public int getMaxCmdTimeout()
	{
		return this.maxCmdTimeout;
	}
	
	
	/*
	 * if path ends with '/' full path will create, else create parent path. 
	 */
	public void mkdirs(SftpClient sftpClient, String path) throws IOException
	{
		path=path.replace('\\', '/');
		List<String> pathSegment=new ArrayList<String>();
		Path parent=Paths.get(path);
		if(!path.endsWith("/") && !path.endsWith("\\"))
			parent=parent.getParent();
		
		Path current=parent;
		while(current!=null)
		{
			try
			{   
				sftpClient.stat(current.toString().replace('\\', '/'));
				break;
			}
			catch(Exception e)
			{
				if(e.getMessage().equals("No such file") && current.getFileName()!=null && !current.getFileName().toString().equals("~"))
					pathSegment.add(current.toString().replace('\\', '/'));
				current=current.getParent();
			}
		}
		
		for(int i=pathSegment.size()-1; i>=0; i--)
			sftpClient.mkdir(pathSegment.get(i));
		
		sftpClient.stat(parent.toString().replace('\\', '/'));
	}
	
	@Override
	public void writeFile(InputStream is, String remote) throws IOException
	{
		SshClient client=SshClient.setUpDefaultClient();
		client.start();
		ClientSession session=null;
		SftpClient sftpClient=null;
		try{
			session=client.connect(remoteUser, host, port).await().getSession();
			if(privKey!=null)
				session.addPublicKeyIdentity(privKey);
			else
				session.addPasswordIdentity(password);
		
			if(!session.auth().await().isSuccess())
				throw new IOException("auth failed");
			
			sftpClient=session.createSftpClient();
			mkdirs(sftpClient, remote);
			
			OutputStream os=sftpClient.write(remote, OpenMode.Create, OpenMode.Write, OpenMode.Truncate);
			StreamsEx.copy(is, os, true);
			
			System.out.println("---write done---");
			
		}finally
		{
			if(sftpClient!=null)
				sftpClient.close();
			if(session!=null)
				session.close();
			client.stop();
		}
	}
	
	@Override
	public void readFile(String remote, OutputStream os) throws IOException
	{
		
		
		SshClient client=SshClient.setUpDefaultClient();
		client.start();
		ClientSession session=null;
		SftpClient sftpClient=null;
		try{
			session=client.connect(remoteUser, host, port).await().getSession();
			if(privKey!=null)
				session.addPublicKeyIdentity(privKey);
			else
				session.addPasswordIdentity(password);
		
			if(!session.auth().await().isSuccess())
				throw new IOException("auth failed");
			
			sftpClient=session.createSftpClient();
			InputStream is=sftpClient.read(remote);
			StreamsEx.copy(is, os, true);
			System.out.println("---read done---");
		}finally
		{
			if(sftpClient!=null)
				sftpClient.close();
			if(session!=null)
				session.close();
			client.stop();
		}
	}
	
	public ExecStatus remoteExec(ClientSession session, String cmdline) throws Exception{
		ChannelExec execChannel=null;
		try{
			execChannel=session.createExecChannel(cmdline);
			ByteArrayOutputStream os=new ByteArrayOutputStream();
			ByteArrayOutputStream es=new ByteArrayOutputStream();
			execChannel.setOut(new LogOutputStream(os, logger, Level.INFO));
			execChannel.setErr(new LogOutputStream(es, logger, Level.ERROR));
			execChannel.open();
			execChannel.waitFor(ClientChannel.CLOSED, maxCmdTimeout);
	
			System.out.println("---exec done: "+execChannel.getExitStatus()+"---");
/*			if(execChannel.getExitStatus()!=0)
				throw new Exception(es.toString());*/
			ExecStatus execStatus=new ExecStatus(os.toString(),es.toString(),execChannel.getExitStatus());
			return execStatus;
		}finally
		{
			if(execChannel!=null)
				execChannel.close();
		}
	}
	  
	@Override
	public ExecStatus remoteExecute(String cmdline) throws Exception 
	{
		SshClient client=SshClient.setUpDefaultClient();
		client.start();
		ClientSession session=null;
		ChannelExec execChannel=null;
		try{
			ConnectFuture cf=client.connect(remoteUser, host, port);
			cf=cf.await();
			session=cf.getSession();
			if(privKey!=null)
				session.addPublicKeyIdentity(privKey);
			else
				session.addPasswordIdentity(password);
		
			AuthFuture future=session.auth();
			if(!future.await().isSuccess())
			{
				Throwable exception=future.getException();
				throw new Exception("auth failed", exception);
			}
			
			execChannel=session.createExecChannel(cmdline);
			ByteArrayOutputStream os=new ByteArrayOutputStream();
			ByteArrayOutputStream es=new ByteArrayOutputStream();
			execChannel.setOut(new LogOutputStream(os, logger, Level.INFO));
			execChannel.setErr(new LogOutputStream(es, logger, Level.ERROR));
			//execChannel.setIn(in);
			execChannel.open();
			
			execChannel.waitFor(ClientChannel.CLOSED, maxCmdTimeout);

			System.out.println("---exec done: "+execChannel.getExitStatus()+"---");
			if(execChannel.getExitStatus()!=0)
				throw new Exception(es.toString());
			ExecStatus execStatus=new ExecStatus(os.toString(),es.toString(),execChannel.getExitStatus());
			return execStatus;
			
		}finally
		{
			if(execChannel!=null)
				execChannel.close();
			if(session!=null)
				session.close();
			client.stop();
		}
	}

	public String getHost() {
		return host;
	}
	public int getPort() {
		return port;
	}
	public String getRemoteUser() {
		return remoteUser;
	}
	public KeyPair getPrivKey() {
		return privKey;
	}
	public String getPassword() {
		return password;
	}
}
