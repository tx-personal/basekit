/**
* @author yangtianxin
* @date 2015-10-15
* Copyright 2015, .... All right reserved. 
*/

package com.xracoon.util.basekit.ssh;

import java.io.InputStream;
import java.io.OutputStream;
import com.xracoon.util.basekit.system.ExecStatus;

public abstract class SSHUtil {
	 public abstract ExecStatus remoteExecute(String cmdline) throws Exception;
	 public abstract void writeFile(InputStream is, String remote) throws Exception;
	 public abstract void readFile(String remote, OutputStream os) throws Exception;
	 //public abstract Object openSession() throws Exception;
	 //public abstract void closeSession(Object session);
	 //public abstract ExecStatus remoteExecute(Object session, String cmdline) throws Exception;
	 
	 public void remoteExec(String cmdline) throws Exception{
		 ExecStatus es=remoteExecute(cmdline);
		 if(es.getRetVal()!=0)
			 throw new Exception("ssh command execute failed!");
	 }
}
