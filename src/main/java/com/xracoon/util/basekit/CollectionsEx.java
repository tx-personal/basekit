package com.xracoon.util.basekit;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CollectionsEx {
	public static <T extends Comparable<? super T>> int distinct(List<T> list) {
		if(list==null)
			return 0;
		
		int n=0;
		Map<T,Integer> helperMap=new HashMap<>();
		for(int i=list.size()-1; i>-1; i--){
			T t=list.get(i);
			Integer lastI=helperMap.put(t, i);
			if(lastI!=null){
				list.remove(lastI.intValue());
				n++;
			}
			
		}
		return n;
	}
	
	public static int indexOfIgnoreCase(Collection<? extends String> list, String key){
		if(list==null)
			return -1;
		
		@SuppressWarnings("unchecked")
		Iterator<String> iter=(Iterator<String>) list.iterator();
		int idx=0;
		while(iter.hasNext()){
			String v=iter.next();
			if(v==null && key==null || v!=null && v.equalsIgnoreCase(key))
				return idx;
			idx++;
		}
		return -1;
	}
	
/*	public static <T> int distinct(List<T> list, Comparator<? super T> c)  {

	}*/
}
