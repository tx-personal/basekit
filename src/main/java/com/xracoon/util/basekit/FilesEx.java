package com.xracoon.util.basekit;
/**
* @author yangtianxin
* @date 2016-6-27
* Copyright 2016, .... All right reserved. 
*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * 注意与Files类的区别
 * nio类
 * 		Path  路径对象
 * 		Paths 创建Path的工具类, 通过URI创建时关联与Scheme对应的FileSystemProvider
 * 		Files Path操作工具类
 * 区别：
 *  	File  平台自身的FileSystem
 *  	nio.Path  可关联不同FileSystemProvider, Path可以完全取代File
 *  	URI   统一资源标识符（抽象），可以是相对的（不包含scheme）
 *  	URL   统一资源定位符（URI的一种实现），一定是绝对的
 *  	URN   统一资源命名，根据名字 唯一标识资源(URI的一种实现)
 *     
 * URI定义
 * [scheme:][//authority][path][?query][#fragment]
 * authority为[user-info@]host[:port]
 */
public class FilesEx {
	private enum SrcType {
		CLASSPATH, FILE, NET
	}

	/**
	 * 
	 * @param path  path/to/file,  classpath:path/to/file, http://path/to/file
	 * @return input stream
	 * @throws IOException IOException
	 */
	public static InputStream openInputStream(String path) throws IOException{
		SrcType srcType=SrcType.FILE;
		path=path.trim();
		if(path.toUpperCase().startsWith("CLASSPATH:")){
			srcType=SrcType.CLASSPATH;
			path=path.substring(10);
		}
		else if(path.contains("://"))
			srcType=SrcType.NET;
		else
			srcType=SrcType.FILE;
		return openInputStream(path, srcType);
	}
	
	/**
	* @param path  path/to/file,  classpath:path/to/file, http://path/to/file
	*/
	public static File getFile(String path) throws URISyntaxException{
		SrcType srcType=SrcType.FILE;
		path=path.trim();
		if(path.toUpperCase().startsWith("CLASSPATH:")){
			srcType=SrcType.CLASSPATH;
			path=path.substring(10);
		}
		else if(path.contains("://"))
			srcType=SrcType.NET;
		else
			srcType=SrcType.FILE;
		return getFile(path, srcType);
	}
	
	/**
	 * 
	 * @param path  path/to/file,  classpath:path/to/file, http://path/to/file
	 * @return list of lines
	 * @throws IOException IOException
	 */
	public static List<String> readLines(String path) throws IOException{
		InputStream inputStream=null;
		try {
			inputStream=openInputStream(path);
			if (inputStream != null) {
				List<String> lines=new LinkedList<>();
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
				String line = null;
				while ((line = reader.readLine()) != null) {
					lines.add(line);
				}
				return lines;
			}
		} catch (IOException e) {
			throw e;
		} finally {
			if (inputStream != null)
				inputStream.close();
		}
		return null;
	}
	
	/**
	 * 
	 * @param path  path/to/file,  classpath:path/to/file, http://path/to/file
	 * @return string of whole content 
	 * @throws IOException IOException
	 */
	public static String readString(String path) throws IOException {
		InputStream inputStream = null;
		StringBuilder content = new StringBuilder();
		try {
			inputStream=openInputStream(path);
			if (inputStream != null) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
				String line = null;

				while ((line = reader.readLine()) != null) {
					content.append(line).append("\n");
				}
				if (content.length() > 0)
					content.deleteCharAt(content.length() - 1);
			}
		} catch (IOException e) {
			throw e;
		} finally {
			if (inputStream != null)
				inputStream.close();
		}

		return content.length() > 0 ? content.toString() : null;
	}
	/**
	 * 
	 * @param path  path/to/file,  classpath:path/to/file, http://path/to/file
	 * @return byte arry of whole content
	 * @throws IOException IOException
	 */
	public static byte[] readBytes(String path) throws IOException {
		InputStream inputStream = null;
		ByteArrayOutputStream baos=null;
		try {
			inputStream=openInputStream(path);
			if (inputStream != null) {
				baos=new ByteArrayOutputStream();
				StreamsEx.copy(inputStream, baos);
			}
		} catch (IOException e) {
			throw e;
		} finally {
			if (inputStream != null)
				inputStream.close();
		}
		return baos!=null?baos.toByteArray():null;
	}
	
	public static void writeFile(byte[] bytes, File file) throws IOException{
		OutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(file);
			outputStream.write(bytes);
		} catch (IOException e) {
			throw e;
		} finally {
			if (outputStream != null)
				outputStream.close();
		}
	}
	public static void writeFile(String content,File file) throws IOException {
		OutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(file);
			if (outputStream != null) {
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
				writer.write(content);
				writer.flush();
				writer.close();
			}
		} catch (IOException e) {
			throw e;
		} finally {
			if (outputStream != null)
				outputStream.close();
		}
	}
	
	public static void writeFile(List<String> lines, File file) throws IOException{
		if(lines==null||lines.size()==0)
			return ;
		String lineSeparator = System.getProperty("line.separator", "\n"); //unix-like: \n ;  windows: \r\n
		OutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(file);
			if (outputStream != null) {
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
				for(String line:lines){
					writer.write(line);
					writer.write(lineSeparator);
				}
				writer.flush();
				writer.close();
			}
		} catch (IOException e) {
			throw e;
		} finally {
			if (outputStream != null)
				outputStream.close();
		}
	}

	private static InputStream openInputStream(String path, SrcType srcType) throws IOException {
		InputStream inputStream = null;
		try {
			if (srcType == SrcType.CLASSPATH)
				inputStream = FilesEx.class.getClassLoader().getResourceAsStream(path);
			else if (srcType == SrcType.FILE)
				inputStream = new FileInputStream(path);
			else if (srcType == SrcType.NET)
				inputStream = new URL(path).openStream();
			
		} catch (IOException e) {
			//file类型找不到，再尝试到classpath查找
			if(srcType==SrcType.FILE && inputStream==null){
				try{
					inputStream = FilesEx.class.getClassLoader().getResourceAsStream(path);
				}catch(Exception e1){
					throw e;
				}
			}
			else
				throw e;
		}
		return inputStream;
	}
	
	private static File getFile(String path, SrcType srcType) throws URISyntaxException {
		File file = null;
		try {
			if (srcType == SrcType.CLASSPATH){
				URL url= FilesEx.class.getClassLoader().getResource(path);
				if(url!=null)
					file = new File(url.toURI());
			}
			else if (srcType == SrcType.FILE)
				file = new File(path);
			else if (srcType == SrcType.NET)
				return null;
			
		} catch (URISyntaxException e) {
			//file类型找不到，再尝试到classpath查找
			if(srcType==SrcType.FILE && file==null){
				try{
					file = new File(FilesEx.class.getClassLoader().getResource(path).toURI());
				}catch(Exception e1){
					throw e;
				}
			}
			else
				throw e;
		}
		return file;
	}
	
	public static boolean deleteTree(File file){
		deleteContent(file);
		return file.delete();
	}
	
	public static boolean deleteContent(File file){
		if(file.isDirectory()){
			for(File f:file.listFiles())
				deleteTree(f);
			return file.listFiles().length==0;
		}
		return false;
	}
	
	/**
	 * make a copy
	 * @param src source
	 * @param dest destPath
	 * @param overwrite is overwrite
	 * @throws IOException IOException
	 */
	public static void copy(File src, File dest, boolean overwrite) throws IOException{
		if(src.isDirectory()){
			if(!dest.exists())
				dest.mkdirs();
			for(File f:src.listFiles()){
				copy(f, new File(dest,f.getName()), overwrite);
			}
		}
		else{
			if(overwrite || !dest.exists()){
				StreamsEx.copy(new FileInputStream(src), new FileOutputStream(dest));
			}
		}
	}
	
	/**
	 * copy source into destination path
	 * @param src source
	 * @param destPath destPath
	 * @param overwrite overwrite
	 * @throws IOException IOException
	 */
	public static void copyTo(File src, File destPath, boolean overwrite) throws IOException{
		copy(src,new File(destPath, src.getName()),overwrite);
	}
	
	/**
	 * copy contents of source into destination path
	 * @param src source
	 * @param destPath destPath
	 * @param overwrite is overwrite
	 * @throws IOException IOException
	 */
	public static void copyContentTo(File src, File destPath, boolean overwrite) throws IOException{
		for(File f:src.listFiles()){
			copy(f, new File(destPath, f.getName()), overwrite);
		}
	}
	
	public static String getBriefSize(long size){
		if(size<1024)
			return size+"B";
		else if(size<1024*1024)
			return Math.round((size*10.0)/1024)/10.0+"K";
		else if(size<1024*1024*1024)
			return Math.round((size*10.0)/1024/1024)/10.0+"M";
		else
			return Math.round((size*10.0)/1024/1024/1024)/10.0+"G";
	}
	
	static public String locateHome(String tagFile){
		File homePath=new File(".").getAbsoluteFile();
		File test=new File(homePath, tagFile);
		if(test.exists())
			return homePath.getAbsolutePath();
		
		String classpath=System.getProperty("java.class.path");
		if(classpath!=null){
			String mainpath= classpath.split(File.pathSeparator)[0];
			if(mainpath.toLowerCase().endsWith(".jar")){
				homePath= new File(mainpath).getParentFile();
				while(homePath!=null){
					test=new File(homePath, tagFile);
					if(test.exists())
						return homePath.getAbsolutePath();
					homePath=homePath.getParentFile();
				}
			}
		}
    	
		return null;
	}

	static public void zip(File basepath, String[] files, File zipfile) throws IOException{
		zip(basepath, files, zipfile, Deflater.DEFAULT_COMPRESSION, null);
	}

	static public void zip(File basepath, String[] files, File zipfile, int level, String comment) throws IOException{
		try(ZipOutputStream zipOs= new ZipOutputStream(new FileOutputStream(zipfile))){
			zipOs.setMethod(ZipEntry.DEFLATED); //ZipEntry.DEFLATED (default), ZipEntry.STORED
			zipOs.setLevel(level);  //level of DEFLATED (-1: Deflater.DEFAULT_COMPRESSION, 0:NO_COMPRESSION,1:BEST_SPEED, ..., 8:DEFLATED, 9:BEST_COMPRESSION)
			if(comment!=null)
				zipOs.setComment(comment);
			for(String relativeFile: files){
				ZipEntry entry = new ZipEntry(relativeFile);
				File in= new File(basepath,relativeFile);
	            zipOs.putNextEntry(entry);
	            if (!in.isDirectory()){
	            	try(InputStream is=new FileInputStream(in)){
	            		StreamsEx.copy(is, zipOs);
	            	}
	            }
	            zipOs.closeEntry();
		    }
		}
	}

	static public void unzip(File zipfile, File output) throws ZipException, IOException{
		try(ZipFile inZip=new ZipFile(zipfile)){
			Enumeration<? extends ZipEntry> entries = inZip.entries();
			while(entries.hasMoreElements()){
				ZipEntry entry= entries.nextElement();
				File outFile= new File(output, entry.getName());
				if(!outFile.getParentFile().exists())
					outFile.getParentFile().mkdirs();
				if(!outFile.exists())
					outFile.createNewFile();
				try(InputStream is=inZip.getInputStream(entry);
						OutputStream os= new FileOutputStream(outFile)){
					StreamsEx.copy(is, os);
				}
			}
		}
	}
}
