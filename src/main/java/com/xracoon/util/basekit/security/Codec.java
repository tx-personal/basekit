package com.xracoon.util.basekit.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Codec {
	private static Logger logger = LoggerFactory.getLogger(Codec.class); 
	
	public static byte[] encBase64(byte[] bytes){
		return encBase64CC(bytes);
	}
	public static byte[] decBase64(byte[] bytes){
		return decBase64CC(bytes);
	}
	
	/**
	 * using org.apache.commons.codec.binary.Base64
	 * 
	 * @param bytes byte array of plain data
	 * @return byte array of encoded data
	 */
	public static byte[] encBase64CC(byte[] bytes){
		org.apache.commons.codec.binary.Base64 base64 = 
				new org.apache.commons.codec.binary.Base64(false);
		try {
			byte[] encoded = base64.encode(bytes);
			return encoded;
		} catch (Exception e) {
			logger.error("base64 encode Exceptoion", e);
			return null;
		}
	}
	
	/**
	 * using org.apache.commons.codec.binary.Base64
	 * 
	 * @param bytes byte array of encoded data
	 * @return byte array of plain data
	 */
	public static byte[] decBase64CC(byte[] bytes){
		org.apache.commons.codec.binary.Base64 base64 = 
				new org.apache.commons.codec.binary.Base64(false);
		try {
			byte[] decoded = base64.decode(bytes);
			return decoded;
		} catch (Exception e) {
			logger.error("base64 decode Exceptoion", e);
			return null;
		}
	}
	
//	/**
//	 * using org.bouncycastle.util.encoders.Base64Encoder
//	 */
//	public static byte[] encBase64BC(byte[] bytes) throws IOException
//	{
//		org.bouncycastle.util.encoders.Base64Encoder decoder = 
//				new org.bouncycastle.util.encoders.Base64Encoder();
//		ByteArrayOutputStream bo=new ByteArrayOutputStream();
//		decoder.encode(bytes, 0, bytes.length, bo);
//		return bo.toByteArray();
//	}
//	
//	/**
//	 * using org.bouncycastle.util.encoders.Base64Encoder
//	 */
//	public static byte[] decBase64BC(byte[] bytes) throws IOException
//	{
//		org.bouncycastle.util.encoders.Base64Encoder decoder = 
//				new org.bouncycastle.util.encoders.Base64Encoder();
//		ByteArrayOutputStream bo=new ByteArrayOutputStream();
//		decoder.decode(bytes, 0, bytes.length, bo);
//		return bo.toByteArray();
//	}
	
	
	/**
	 * Just can be used for sun/orcale Java only, not compatible with other JAVA implements
	 * 
	 * @param bytes byte array of plain data
	 * @return byte array of encoded data
	 */
	@Deprecated
	public static byte[] encBase64Sun(byte[] bytes){
		try {
			// byte[]|ByteBuffer|InputStream -> String|OuputStream
			@SuppressWarnings("restriction")
			byte[] encoded = new sun.misc.BASE64Encoder().encodeBuffer(bytes).getBytes();
			return encoded;
		} catch (Exception e) {
			logger.error("base64 encode Exceptoion", e);
			return null;
		}
	}
	
	/**
	 * Just can be used for sun/orcale Java only, not compatible with other JAVA implements
	 * 
	 * @param bytes byte array of encoded data
	 * @return byte array of plain data
	 */
	@Deprecated
	public static byte[] decBase64Sun(byte[] bytes){
		try {
			// String|InputStream -> byte[]|ByteBuffer|OutputStream
			@SuppressWarnings("restriction")
			byte[] decoded =  new sun.misc.BASE64Decoder().decodeBuffer(new String(bytes));
			return decoded;
		} catch (Exception e) {
			logger.error("base64 decode Exceptoion", e);
			return null;
		}
	}
}
