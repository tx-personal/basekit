package com.xracoon.util.basekit.security;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Digest {
	private static Logger logger = LoggerFactory.getLogger(Digest.class); 
	
	//消息摘要算法包含MD、SHA和MAC三大系列，MAC与MD和SHA不同， MAC是含有密钥的散列函数算法，我们也常把MAC称为HMAC。
	//JDK6支持MD2/MD5/SHA(SHA-1)/SHA-256/SHA-384/SHA-512    /HmacMD5/HmacSHA1/HmacSHA256/HmacSHA384/HmacSHA512

	//参考： http://blog.163.com/yxhui_cool/blog/static/770197702012291433339/
	
	/**
	 * message digest
	 * @param bytes byte array of content to digest
	 * @param algorithm MD/SHA algorithm, supported by JDK6: MD2/MD5/SHA(SHA1)/SHA256/SHA384/SHA512
	 * @return byte array of digest data
	 */
	static public byte[] digest(byte[] bytes, String algorithm){
		try {
			MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
	        messageDigest.update(bytes);  
	        return messageDigest.digest(); 
		} catch (NoSuchAlgorithmException e) {
			logger.error("digest error", e);
			return null;
		}  
	}
	
	/**
	 * message digest with secret key.  Use KeyGenerater generate a new secret key
	 * @param bytes byte array of content to digest
	 * @param algorithm MAC algorithm, supported by JDK6: HmacMD5/HmacSHA1/HmacSHA256/HmacSHA384/HmacSHA512
	 * @return byte array of digest data
	 */
	public static byte[] digest(byte[] bytes, String algorithm, byte[] key){
		try {
			Mac mac = Mac.getInstance(algorithm);
	        mac.init(new SecretKeySpec(key, algorithm));
	        mac.update(bytes);
	        return mac.doFinal();
		} catch (NoSuchAlgorithmException | InvalidKeyException e) {
			logger.error(algorithm+" hmac Exceptoion", e);
			return null;
		}
	}
}
