package com.xracoon.util.basekit.security;

import java.io.IOException;
import java.io.StringReader;
import java.security.KeyPair;
import java.security.PublicKey;
import java.security.Security;

import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMDecryptorProvider;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CryptoBC {
	private static Logger logger = LoggerFactory.getLogger(Crypto.class); 
	
	static{
		Security.addProvider(new BouncyCastleProvider());
	}

	//创建pem格式的keypair
	//......
	
	public static PublicKey readPubkeyPEM(String content) throws IOException{
		
		Object object = readPEMObject(content);
		if(object==null)
			return null;
		if(object instanceof SubjectPublicKeyInfo){
			JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
			return converter.getPublicKey((SubjectPublicKeyInfo)object);
		}
		return null;
	}
	
	private static Object readPEMObject(String content) throws IOException{
		PEMParser parser=null;
		try{
			parser = new PEMParser(new StringReader(content));
			Object object = parser.readObject();
			return object;
	    }finally{
	    	if(parser!=null)
	    		parser.close();
	    }
	} 
	
	public static KeyPair readKeyPairPEM(String content, char[] passwd) throws IOException{
		//older version
		//PEMReader reader = new PEMReader(new StringReader(content), new Password (passwd), "BC");
		//KeyPair keyPair = (KeyPair) reader.readObject();
		
		//1.48 开始不再使用PEMReader，改用PEMParser
		Object object = readPEMObject(content);
		if(object==null)
			return null;
		
		if(object instanceof PEMKeyPair ||object instanceof PEMEncryptedKeyPair){
		    JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
		    KeyPair kp;
		    if (object instanceof PEMEncryptedKeyPair) {
		    	logger.info("Encrypted key - we will use provided password");
		    	PEMDecryptorProvider decProv = new JcePEMDecryptorProviderBuilder().build(Crypto.passwdFix(passwd));
		        kp = converter.getKeyPair(((PEMEncryptedKeyPair) object).decryptKeyPair(decProv));
		    } else {
		    	logger.info("Unencrypted key - no password needed");
		        kp = converter.getKeyPair((PEMKeyPair) object);
		    }
		    return kp;
	    }
		else if(object instanceof PEMKeyPair){
			
		}
		return null;
	}
}
