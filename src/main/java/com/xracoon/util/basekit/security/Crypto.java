package com.xracoon.util.basekit.security;

import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.DSAParams;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.DSAPublicKeySpec;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * implements by JCE
 */
public class Crypto {
//	static{
//		Security.addProvider(new com.sun.crypto.provider.SunJCE());
//	}
	private static Logger logger = LoggerFactory.getLogger(Crypto.class); 
	
	static public char[] passwdFix(char[] passwd){
		return passwd==null?new char[0]:passwd;
	}
	static public char[] passChars(String passwd){
		char[] nPassword = new char[0];
		if (passwd != null && passwd.trim().length()>0)
			nPassword = passwd.toCharArray();
		return nPassword;
	}
	
	/**
	 * MAC Digest algorithm :  HmacMD5/HmacSHA1/HmacSHA256/HmacSHA384/HmacSHA512
	 * symmetry cryptography algorithm: DES/DESede(DES3)/AES/IDEA/RC2/RC4/RC5/SKIPJACK
	 * 
	 * AES秘钥默认最多只支持128位，如需更高位数需则要改动java策略配置
	 * Cipher.getInstance("DES/CBC/PKCS5Padding");   
	 * 
	 * @param algorithm algorithm
	 * @param size key size
	 * @param algorithmParam algorithm parameter
	 * @return securteKey
	 */
	 static public SecretKey newSecretKey(String algorithm,Integer size, AlgorithmParameterSpec algorithmParam) {
		try {
			KeyGenerator keyGen=KeyGenerator.getInstance(algorithm);
			if(size!=null)
				keyGen.init(size);
			if(algorithmParam!=null)
				keyGen.init(algorithmParam);
			SecretKey secretKey=keyGen.generateKey();
			secretKey.getEncoded();
			return secretKey;
		} catch (NoSuchAlgorithmException|InvalidAlgorithmParameterException e) {
			logger.error("secret key generate failed", e);
			return null;
		}
	}
	
	static public SecretKey loadSecretKey(byte[] bytes, String algorithm){
		return new SecretKeySpec(bytes, algorithm);
	}
	
	//-------symmetry cryptography--------
	static public byte[] encrypt(SecretKey key, byte[] content) throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException{
		Cipher cipher=Cipher.getInstance(key.getAlgorithm()); 
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] secretcontent=cipher.doFinal(content);
		return secretcontent;
	}
	
	static public byte[] decrypt(SecretKey key, byte[] secretcontent) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		Cipher cipher=Cipher.getInstance(key.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] content=cipher.doFinal(secretcontent);
		return content;
	}
	
	/**
	 * 
	 * @param bytes byte array of plain data
	 * @param algorithm algorithm: AES, DES, AES/CBC/PKCS5Padding, 
	 * 		不带模式和填充时默认值为/ECB/PKCS5Padding, 
	 *	 	JCE支持的模式 CBC, CFB, ECB, OFB,PCBC, 
	 * 		JCE支持的填充： NoPadding, PKCS5Padding, ISO10126Padding, 
	 * 		默认的IV为0数组: 
	 * 		 AES-128(默认，16位), AES-192(24位), AES-256(32位) 
	 * @param key byte array of key
	 * @param algorArgs byte array of algorithm args
	 * @return byte array of encrypted data
	 */
	public static byte[] encrytSymm(byte[] bytes, String algorithm, byte[] key, byte[]...algorArgs){
		return cryptoSymm(Cipher.ENCRYPT_MODE, bytes,algorithm, key, algorArgs);
	}
	
	/**
	 * 
	 * @param bytes byte array of encrypted data
	 * @param algorithm algorithm: AES, DES, AES/CBC/PKCS5Padding, 
	 * 		不带模式和填充时默认值为/ECB/PKCS5Padding, 
	 * 		JCE支持的模式 CBC, CFB, ECB, OFB,PCBC, 
	 * 		JCE支持的填充： NoPadding, PKCS5Padding, ISO10126Padding, 
	 * 		默认的IV为0数组: 
	 * 		 AES-128(默认，16位), AES-192(24位), AES-256(32位)
	 * @param key byte array of key
	 * @param algorArgs byte array of algorithm args
	 * @return byte array of plain data
	 */
	public static byte[] decrytSymm(byte[] bytes, String algorithm, byte[] key, byte[]...algorArgs){
		return cryptoSymm(Cipher.DECRYPT_MODE, bytes,algorithm, key, algorArgs);
	}
	
	
	public static byte[] cryptoSymm(int model, byte[] bytes, String algorithm, byte[] key, byte[]...algorArgs){
		try {
			String algorBase= algorithm.contains("/")?algorithm.substring(0,algorithm.indexOf("/")):algorithm;
			SecretKeySpec skeySpec = new SecretKeySpec(key, algorBase);
			Cipher cipher = Cipher.getInstance(algorithm);//"算法/模式/补码方式"
			if(model==Cipher.ENCRYPT_MODE && algorBase.equalsIgnoreCase("AES") && algorithm.toUpperCase().contains("/NOPADDING")){
				int len=(int)Math.ceil(bytes.length/16.0)*16;
				bytes= Arrays.copyOf(bytes, len);
			}
			if(algorithm.toUpperCase().contains("/CBC")){
				IvParameterSpec iv = new IvParameterSpec(algorArgs[0]);//使用CBC模式，需要一个向量iv，可增加加密算法的强度  
				cipher.init(model, skeySpec, iv);
			}
			else{
				cipher.init(model, skeySpec);
			}
			byte[] result = cipher.doFinal(bytes);
			return result;
		} catch (Exception e) {
			logger.error(algorithm+" mode "+model+" decode Exceptoion", e);
			return null;
		}
	}
	

	/**
	 * 非对称加密算法：
	 * 	DSA, RSA
	 * 签名算法：
	 * 	RawDSA, SHA1withDSA
	 * 	MD2withRSA, MD5withRSA, SHA1withRSA, SHA256withRSA, SHA384withRSA, SHA512withRSA, RSASignature, P11Signature
	 * 
	 * @param algorithm algorithm
	 * @param size key size
	 * @param algorithmParam algorithm param
	 * @return key pair
	 */
	static public KeyPair newKeyPair(String algorithm,Integer size, AlgorithmParameterSpec algorithmParam) {   
       try{
			KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(algorithm);   
	        
	        //SecureRandom secureRandom=new SecureRandom();
	        //secureRandom.setSeed("xxxx".getBytes());
	        //keyPairGen.initialize(1024, secureRandom); 
				
	        if(size!=null)
	        	keyPairGen.initialize(1024);  
	        if(algorithmParam!=null)
	        	keyPairGen.initialize(algorithmParam);
	       
	        KeyPair keyPair = keyPairGen.generateKeyPair();   
	        
	        //RSAPrivateKey privateKey = (RSAPrivateKey)keyPair.getPrivate();  
	        //RSAPublicKey publicKey = (RSAPublicKey)keyPair.getPublic();
	        return keyPair;  
		} catch (NoSuchAlgorithmException|InvalidAlgorithmParameterException e) {
			logger.error("keypair generate failed", e);
			return null;
		}
    } 
	
	static public byte[] encrypt(PublicKey pubkey, byte[] content) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		Cipher cipher = Cipher.getInstance(pubkey.getAlgorithm()); 
		cipher.init(Cipher.ENCRYPT_MODE, pubkey);
		byte[] encodeBytes=cipher.doFinal(content);
		return encodeBytes;
	}
	
	static public byte[] decrypt(PrivateKey prvkey, byte[] secretcontent) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		Cipher cipher=Cipher.getInstance(prvkey.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, prvkey);
		byte[] origincontent=cipher.doFinal(secretcontent);
		return origincontent;
	}
	
	static public byte[] sign(String signAlgorithm, PrivateKey priKey, byte[] contents) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException{
		Signature dsa=Signature.getInstance(signAlgorithm);
		dsa.initSign(priKey);
		dsa.update(contents);
		byte[] signedContent=dsa.sign();
		return signedContent;
	}
	
	static public boolean verify(String signAlgorithm, PublicKey prikey, byte[] data, byte[] sign) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException{
		Signature signature = Signature.getInstance(signAlgorithm);   
        signature.initVerify(prikey);   
        signature.update(data);   
        return signature.verify(sign);   
	}
	
	static public String trimDesc(List<String> lines){
		StringBuilder sb=new StringBuilder();
		for(String line: lines){
			if(!line.startsWith("---"))
				sb.append(line).append("\n");
		}
		if(sb.length()>0)
			sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}
	
	/**
	 * read binary data of private keys (PKCS8)
	 * 
	 * @param algorithm algorithm
	 * @param bytes byte array of private key
	 * @return private key
	 */
	static public PrivateKey readPrikeyDER(String algorithm, byte[] bytes){
		try{
			EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(bytes);
			KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
			PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
			return privateKey;
		} catch (NoSuchAlgorithmException|InvalidKeySpecException e) {
			logger.error("private key load failed", e);
			return null;
		}
	}
	
	/**
	 *  read binary data of public keys(OpenSSL), public key of OpenSSH need a translate 
	 *  
	 * @param algorithm algorithm
	 * @param keyBytes bytes byte array of public key
	 * @return public key
	 */
	static public PublicKey readPubkeyDER(String algorithm, byte[] keyBytes){
		try{
			 KeySpec keySpec = new X509EncodedKeySpec(keyBytes);  
			 KeyFactory keyFactory = KeyFactory.getInstance(algorithm);  
			 return keyFactory.generatePublic(keySpec); 
		} catch (NoSuchAlgorithmException|InvalidKeySpecException e) {
			logger.error("public key load failed", e);
			return null;
		}
	}
	
	/**
	 * read rsa public key of OpenSSH format(like 'ssh-rsa BASE64CONTENT comment'). Besides, OpenSSH use PEM format private key
	 * 
	 * @param content string content of ssh public key
	 * @return public key
	 * @throws InvalidKeySpecException exception
	 * @throws NoSuchAlgorithmException exception
	 */
	static public PublicKey readPubKeySSH(String content) throws InvalidKeySpecException, NoSuchAlgorithmException{
		String[] parts=content.trim().split("\\s+");
		if(parts[0].equalsIgnoreCase("ssh-rsa")){
			byte[] bytes=Codec.decBase64(parts[1].getBytes());
			if(Arrays.equals(Arrays.copyOf(bytes, 11), new byte[]{0,0,0,7,'s','s','h','-','r','s','a'})){
				int pubexpLen=new BigInteger(Arrays.copyOfRange(bytes, 11, 15)).intValue();
				int idx=15+pubexpLen;
				BigInteger pubexp=new BigInteger(Arrays.copyOfRange(bytes, 15, idx));
				int modulesLen=new BigInteger(Arrays.copyOfRange(bytes, idx, idx+4)).intValue();
				BigInteger modules=new BigInteger(Arrays.copyOfRange(bytes, idx+4, idx+4+modulesLen));
				if(bytes.length!=idx+4+modulesLen)
					return null;
				
				RSAPublicKeySpec pbkey=new RSAPublicKeySpec(modules, pubexp);
				return KeyFactory.getInstance("RSA").generatePublic(pbkey);
			}
		}
		else if(parts[0].equalsIgnoreCase("ssh-dss")){
			byte[] bytes=Codec.decBase64(parts[1].getBytes());
			if(Arrays.equals(Arrays.copyOf(bytes, 11), new byte[]{0,0,0,7,'s','s','h','-','d','s','s'})){
				//y, p, q, g
				int pLen=new BigInteger(Arrays.copyOfRange(bytes, 11, 15)).intValue();
				int idx=15+pLen;
				BigInteger p=new BigInteger(Arrays.copyOfRange(bytes, 15, idx));
				
				int qLen= new BigInteger(Arrays.copyOfRange(bytes, idx, idx+4)).intValue();
				BigInteger q=new BigInteger(Arrays.copyOfRange(bytes, idx+4, idx+4+qLen));
				idx=idx+4+qLen;
				
				int gLen= new BigInteger(Arrays.copyOfRange(bytes, idx, idx+4)).intValue();
				BigInteger g=new BigInteger(Arrays.copyOfRange(bytes, idx+4, idx+4+gLen));
				idx=idx+4+gLen;
				
				int yLen= new BigInteger(Arrays.copyOfRange(bytes, idx, idx+4)).intValue();
				BigInteger y=new BigInteger(Arrays.copyOfRange(bytes, idx+4, idx+4+yLen));
				idx=idx+4+yLen;
				
				if(bytes.length!=idx)
					return null;
				
				DSAPublicKeySpec pbkey= new DSAPublicKeySpec(y, p, q, g);  //(BigInteger y, BigInteger p, BigInteger q, BigInteger g)
				return KeyFactory.getInstance("DSA").generatePublic(pbkey);
			}
		}
		
		return null;
	}
	
	/**
	 * generate public key by private key, 
	 * only use for JCE with RSA,DSA
	 * 
	 * @param privKey private key
	 * @return public key
	 */
	public static PublicKey getPubkey(PrivateKey privKey){
		try{
			//sun.security.rsa.RSAKeyPairGenerator
			//RSAPublicKeyImpl, RSAPrivateCrtKeyImpl
			if(privKey instanceof RSAPrivateCrtKey){
				RSAPrivateCrtKey pvkey=(RSAPrivateCrtKey)privKey;
				RSAPublicKeySpec pbkey=new RSAPublicKeySpec(pvkey.getModulus(),pvkey.getPublicExponent());
				return KeyFactory.getInstance("RSA").generatePublic(pbkey);
			}
			//sun.security.provider.DSAKeyPairGenerator
			//DSAPublicKey(y, p, q, g) (SERIAL_INTEROP),  DSAPublicKeyImpl(y, p, q, g),    DSAPrivateKey(x, p, q, g)
			else if(privKey instanceof DSAPrivateKey){
				DSAPrivateKey pvkey=(DSAPrivateKey)privKey;
				DSAParams param=pvkey.getParams();
				BigInteger y =  param.getG().modPow(pvkey.getX(), param.getP()); // y = g.modPow(x, p);
				DSAPublicKeySpec pbkey= new DSAPublicKeySpec(y,param.getP(),param.getQ(),param.getG());  //(BigInteger y, BigInteger p, BigInteger q, BigInteger g)
				return KeyFactory.getInstance("DSA").generatePublic(pbkey);
			}
			
			return null;
		} catch (NoSuchAlgorithmException|InvalidKeySpecException e) {
			logger.error("public key get failed", e);
			return null;
		}
	}
}
