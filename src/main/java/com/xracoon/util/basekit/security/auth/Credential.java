package com.xracoon.util.basekit.security.auth;

import java.io.IOException;
import java.security.PrivateKey;
import java.util.Arrays;

import com.xracoon.util.basekit.security.Crypto;
import com.xracoon.util.basekit.security.CryptoBC;

public class Credential {
	public enum CredType{
		PASSWORD,
		PRIVKEY
	}
	
	private String user;
	private char[] passwd;
	private PrivateKey privKey;
	private CredType credType;
	
	//redundant info
	private String pemKey;
	
	public void clean(){
		if(passwd!=null)
			Arrays.fill(passwd, (char)-1);
	}
	public static Credential Plain(String user, char[] passwd){
		Credential ident=new Credential();
		ident.user=user;
		ident.passwd=Crypto.passwdFix(passwd);
		ident.credType=CredType.PASSWORD;
		return ident;
	}

	public static Credential SecretKeyPem(String user, String pemKey, char[] passwd) throws IOException{
		Credential ident=new Credential();
		ident.user=user;
		ident.pemKey=pemKey;
		ident.passwd=Crypto.passwdFix(passwd);
		ident.privKey=CryptoBC.readKeyPairPEM(pemKey, passwd).getPrivate();
		ident.credType=CredType.PRIVKEY;
		return ident;
	}
	public static Credential SecretKey(String user, byte[] keys, String algorithm){
		Credential ident=new Credential();
		ident.user=user;
		ident.privKey=Crypto.readPrikeyDER(algorithm, keys);
		ident.credType=CredType.PRIVKEY;
		return ident;
	}
	public static Credential SecretKey(String user, PrivateKey priKey){
		Credential ident=new Credential();
		ident.user=user;
		ident.privKey=priKey;
		ident.credType=CredType.PRIVKEY;
		return ident;
	}
	public static Credential SecretKey(String user, PrivateKey priKey, char[] passwd){
		Credential ident=new Credential();
		ident.user=user;
		ident.privKey=priKey;
		ident.credType= CredType.PRIVKEY;
		ident.passwd= Crypto.passwdFix(passwd);
		return ident;
	}
	
	public String getUser() {
		return user;
	}
	public char[] getPasswd() {
		return passwd;
	}
	public PrivateKey getSecretkey() {
		return privKey;
	}
	public String getPemKey(){
		return pemKey;
	}
	public boolean isSecretKey(){
		return credType==CredType.PRIVKEY;
	}
}
