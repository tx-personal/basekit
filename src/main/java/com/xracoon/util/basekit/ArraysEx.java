package com.xracoon.util.basekit;

import java.util.HashMap;
import java.util.Map;

public class ArraysEx {
	/**
	 * @param array array
	 * @param delimiter delimiter, default delimiter is ','
	 * @return joined string
	 */
	public static String join(Object[] array, String delimiter){
		if(array==null)
			return null;
		
		if(delimiter==null)
			delimiter=",";
		StringBuilder stringBuilder=new StringBuilder();
		for(Object o: array){
			stringBuilder.append(o.toString()).append(delimiter);
		}
		int len=stringBuilder.length();
		if(len>delimiter.length())
			stringBuilder.delete(len-delimiter.length(), stringBuilder.length());
		return stringBuilder.toString();
	}

	/**
	 * 
	 * @param keyval key-value-key-value list, Map&lt;String,String&gt; a=ArraysEx.toMap("key1", "value1", "key2", "value2");
	 * @return map
	 */
	public static Map<String,String> toMap(String ...keyval){
		Map<String, String> map= new HashMap<>();
		for(int i=0; i+1<keyval.length; i=i+2){
			map.put(keyval[i], keyval[i+1]);
		}
		return map;
	}
}
