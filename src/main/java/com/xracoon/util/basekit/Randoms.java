package com.xracoon.util.basekit;

import java.security.SecureRandom;
import java.util.Random;
import java.util.UUID;

public class Randoms {
	private static Random rand=new Random();
	//private static SecureRandom secureRand= new SecureRandom();
	
	public static int randomBiasIndex(int len, int power){
		double m = Math.ceil(len/2.0); 
		double s = m-1; 

		int idx= rand.nextInt(len);
		if(idx >= len-s){
			double p=(s-(len-idx-1))/m;
			if(Math.pow(rand.nextDouble(),power)<p){
				idx=len-idx-1;
			}
		}
		return idx;
	}
	
	public static String uuid(String delimiter){
		String uuid=UUID.randomUUID().toString();
		if(delimiter!=null)
			uuid=uuid.replaceAll("-", delimiter);
		return uuid;
	}
	
	public static String randomString(int len){
		StringBuilder sb=new StringBuilder();
		for(int i=0; i<len; i++)
			sb.append(rand.nextInt(255));
		return sb.toString();
	}
	
	private static byte[] random(int len, byte low, byte high){
		byte[] bytes= new byte[Math.max(len,0)];
		for(int i=0; i<len; i++)
			bytes[i] = (byte)(low+rand.nextInt(high-low));
		return bytes;
 	}
	public static String randomNumber(int len){
		return null;
	}
	public static String randomHex(int len){
		return null;
	}
	public static String randomAlphabet(int len){
		return null;
	}
	public static String randomAlphabetNumber(int len){
		return null;
	}
}