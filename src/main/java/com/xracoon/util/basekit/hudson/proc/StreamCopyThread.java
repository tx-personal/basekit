/*
 * The MIT License
 * 
 * Copyright (c) 2004-2009, Sun Microsystems, Inc., Kohsuke Kawaguchi
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.xracoon.util.basekit.hudson.proc;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link Thread} that copies {@link InputStream} to {@link OutputStream}.
 *
 * @author Kohsuke Kawaguchi
 */
public class StreamCopyThread extends Thread {
    private BufferedReader reader;
    private final Writer writer;
    private final boolean closeOut;
    private final String lineSeparator=System.getProperty("line.separator");
    private Logger logger=LoggerFactory.getLogger(StreamCopyThread.class);
    
    public StreamCopyThread(String threadName, InputStream in, OutputStream out, boolean closeOut, Logger logger)  {
        super(threadName);
    	if(File.pathSeparator.equals(";")){
			try {
				this.reader = new BufferedReader(new InputStreamReader(in,"GBK"));
			} catch (UnsupportedEncodingException e) {
				this.reader = new BufferedReader(new InputStreamReader(in));
			}
    	}
		else
			this.reader = new BufferedReader(new InputStreamReader(in));
    	
        if (out == null) out=new NullStream();
        this.writer = new OutputStreamWriter(out);
        this.closeOut = closeOut;
        this.logger=logger;
    }

    public StreamCopyThread(String threadName, InputStream in, OutputStream out, Logger logger) {
        this(threadName,in,out,false, logger);
    }

    @Override
    public void run() {
        try {
            try {
                String line=null;
                while ((line = reader.readLine()) != null){
                	writer.write(line);
                    writer.write(lineSeparator);
                    writer.flush();
                    if(logger!=null)
                    	logger.info(line);
                }
            } finally {
                // it doesn't make sense not to close InputStream that's already EOF-ed,
                // so there's no 'closeIn' flag.
            	reader.close();
                if(closeOut)
                	writer.close();
            }
        } catch (IOException e) {
            // TODO: what to do?
        	System.out.println("Exception in StreamCopyThread: "+e.getMessage());
        }
    }
}