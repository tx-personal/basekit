package com.xracoon.util.basekit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringEx {
	public static String trimLeft(String str, char c){
		if(str==null) return str;
		
		char[] chars=str.toCharArray();
		int start=0;
		while(start<chars.length && chars[start]==c)
			start++;
		if(start==chars.length)
			return "";
		return new String(chars,start,chars.length-start);
	}
	
	public static String trimRight(String str, char c){
		if(str==null) return str;
		
		char[] chars=str.toCharArray();
		int end=chars.length-1;
		while(end>-1 && chars[end]==c)
			end--;
		if(end<0)
			return "";
		return new String(chars,0,end+1);
	}
	
	public static String trim(String str,char c){
		if(str==null) return str;
		
		char[] chars=str.toCharArray();
		int start=0,end=chars.length-1;
		while( start<chars.length && chars[start]==c)
			start++;
		if(start==chars.length)
			return "";
		while( end>-1 && chars[end]==c)
			end--;
		return new String(chars,start,end-start+1);
	}
	
	static public byte[] hex2Bytes(String hexString)
	{
		if(StringEx.isBlank(hexString))
			return null;
        int len = hexString.length() / 2;  
        byte[] ret = new byte[len];  
        for (int i = 0; i < len; i++) {  
            ret[i] = (byte) Integer.valueOf(hexString.substring(i * 2, i * 2 + 2), 16).byteValue();  
        }  
        return ret;  
	}
	
	static public String bytes2Hex(byte[] bytes)
	{
		StringBuilder stringBuilder = new StringBuilder();       
        if (bytes == null || bytes.length <= 0) {       
            return null;       
        }       
        for (int i = 0; i < bytes.length; i++) 
        {  
            int v = bytes[i] & 0xFF;       
            String hv = Integer.toHexString(v);       
            if (hv.length() < 2) {       
                stringBuilder.append(0);       
            }       
            stringBuilder.append(hv);       
        }       
        return stringBuilder.toString();       
    }
	
	public static String emptyFix(String string){
		return string==null?"":string;
	}

	public static boolean isBlank(String string){
		return string==null||string.trim().length()==0;
	}
	public static boolean isNumber(String string){
		return !isBlank(string) && string.matches("\\d+(\\.\\d+)?");
	}
	public static boolean isHex(String string){
		return !isBlank(string) && string.matches("[a-fA-F0-9]+(\\.[a-fA-F0-9]+)?");
	}
	public static String reverse(String string){
		if(string==null)
			return null;
		char[] arrays=string.toCharArray();
		StringBuilder sb=new StringBuilder();
		for(int i=arrays.length-1; i>-1; i--)
			sb.append(arrays[i]);
		return sb.toString();
	}
	
	/**
	 * 无法反向引 
	 * @param input input string
	 * @param tokenMap token map
	 * @return return token resolved string
	 */
	public static String resolveToken(String input, Map<String, String> tokenMap){
		if(tokenMap==null || tokenMap.size()==0)
			return input;
		
		//unify token to upper case
		Map<String, String> innerMap= new HashMap<>();
		for(Entry<String,String> e: tokenMap.entrySet())
			innerMap.put(e.getKey().toUpperCase(), e.getValue());
		
		Pattern p=Pattern.compile("\\$\\{[ \\t]*([a-zA-Z0-9_\\.-]+)[ \\t]*\\}|\\$([a-zA-Z0-9_\\.-]+)(?=[ \\t]|$)", Pattern.CASE_INSENSITIVE);
		Matcher m= p.matcher(input);
		char[] chars=input.toCharArray();
		StringBuffer sb=new StringBuffer();
		int curIdx=0;
		while(m.find()){
			String matchGroup=m.group(1)!=null?m.group(1):m.group(2);
			String tokenValue=innerMap.get(matchGroup.toUpperCase());
			sb.append(chars, curIdx, m.start()-curIdx);
			if(tokenValue!=null){
				//m.appendReplacement(sb, tokenValue);
				sb.append(tokenValue);
			}
			else
				sb.append(chars, m.start(), m.end()-m.start());
			curIdx=m.end();
		}
		sb.append(chars, curIdx, chars.length-curIdx);
		return sb.toString();
	}
	
	public static List<String> tokenize(String cmdline){
		List<String> cmdPart=new ArrayList<>();
		StringBuilder sb=new StringBuilder();
		char[] chars=cmdline.toCharArray();
		for(int i=0, len=chars.length; i<len; i++){
			char c=chars[i];
			if(c==' '||c=='\t'||c=='\r'||c=='\n'){
				String part= sb.toString();
				sb=new StringBuilder();
				if(part.length()>0)
					cmdPart.add(part);
			}
			else if(c=='\'' || c=='"'){
				char quote=c;
				i++; //remove quote
				while(i<len && chars[i]!=quote)
					sb.append(chars[i++]);
			}
			else
				sb.append(chars[i]);
		}
		cmdPart.add(sb.toString());
		return cmdPart;
	}
	
	public static String dateRangeDesc(long durationMs){
		String desc= "";
		long ms=durationMs%1000;
		long secAll=durationMs/1000;
		long minAll=secAll/60;
		long sec=secAll%60;
		if(secAll<=0)
			return durationMs+"ms";
		if(secAll>0)
			desc= sec+"s";
		if(minAll<=0)
			return desc+" "+ms+"ms";
		
		long hourAll= minAll/60;
		long min=minAll%60;
		if(minAll>0)
			desc= min+"m "+desc;
		if(hourAll<=0)
			return desc;
		
		long dayAll=hourAll/24;
		long hour=hourAll%24;
		if(hourAll>0)
			desc= hour+"h "+desc;
		if(dayAll<=0)
			return desc;
		desc= dayAll+"d "+desc;
		return desc;
	}
}
