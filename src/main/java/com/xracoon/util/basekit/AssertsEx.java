package com.xracoon.util.basekit;

public class AssertsEx {
	/**
	 * <pre>Math.abs(expect - actual) &lt; precision </pre>
	 * @param expect expect value
	 * @param actual actual value
	 * @param precision precision of comparison
	 */
	public static void assertEqualReal(double expect, double actual, double precision){
		if(!(Math.abs(expect-actual)<precision))
			throw new AssertionError("except:"+expect+" -  actual:"+actual+" = "+(expect-actual)+" < precision:"+precision);
	}
}
