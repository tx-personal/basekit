package com.xracoon.util.basekit.net;

import java.util.Arrays;

import com.xracoon.util.basekit.ArraysEx;
import com.xracoon.util.basekit.StringEx;

public class IPv4 extends IP {
	public static final String localhost = "127.0.0.1"; // 点分十进制, 共32位，分4段，每段8位
	private short[] section = new short[4];

	public IPv4(){};
	public IPv4(String ip) throws IllegalIPv4AddressException{
		parse(ip);
	}
	@Override
	public byte getVersion() {
		return IPv4;
	}
	@Override
	public IPv4 parse(String ip) throws IllegalIPv4AddressException {
		if (StringEx.isBlank(ip))
			throw new IllegalIPv4AddressException(ip);
		String[] parts = ip.split("\\.");
		if (parts.length != 4)
			throw new IllegalIPv4AddressException(ip);
		
		for (int i = 0; i < parts.length; i++) {
			String s = parts[i];
			if (!s.matches("\\d{1,3}"))
				throw new IllegalIPv4AddressException(ip);
			int n = -1;
			try {
				n = Integer.parseInt(s);
			} catch (Exception e) {
				throw new IllegalIPv4AddressException(ip);
			}
			if (n < 0 || n > 255)
				throw new IllegalIPv4AddressException(ip);
			section[i] = (byte) n;
		}
		return this;
	}
	
	/**
	 * <div>10.0.0.0 ~ 10.255.255.255,</div>
	 * <div>172.16.0.0 ~ 172.31.255.255,</div>
	 * <div>192.168.0.0 ~ 192.168.255.255</div>
	 */
	@Override
	public boolean isPrivateAddress() {
		return section[0]==10 ||
				section[0]==172 && section[1]>=16 && section[1]<=31 ||
				section[0]==192 && section[1]==168;
	}
	
	@Override
	public String toString(){
		return String.format("%d.%d.%d.%d", 
				section[0],section[1],section[2],section[3]);
	}
	@Override
	public boolean equals(Object obj) {
		if(obj==this)
			return true;
		if(obj instanceof IPv4)
			return Arrays.equals(((IPv4)obj).section, section);
		return false;
	}
	@Override
	public int hashCode(){
		return toString().hashCode();
	}
	public static class IllegalIPv4AddressException extends IllegalIPAddressException {
		private static final long serialVersionUID = 6389038197546439161L;

		public IllegalIPv4AddressException(String message) {
			super(message);
		}

		public IllegalIPv4AddressException(String message, Throwable cause) {
			super(message, cause);
		}
	}
}
