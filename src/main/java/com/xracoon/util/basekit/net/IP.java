package com.xracoon.util.basekit.net;

public abstract class IP {
	public static final byte IPv4= 4;
	public static final byte IPv6= 6;
	
	abstract public IP parse(String ip) throws IllegalIPAddressException;
	abstract public boolean isPrivateAddress();
	abstract public byte getVersion();
	public static class IllegalIPAddressException extends Exception{
		private static final long serialVersionUID = -1063596349181565758L;
		public IllegalIPAddressException(String message){
			super(message);
		}
		public IllegalIPAddressException(String message, Throwable cause){
			super(message, cause);
		}
	}
	
	//helper
	public static IP parseIP(String ipString){
		IP ip=null;
		try{
			if(ipString.contains(":")){
				ip=new IPv6(ipString);
			}else
				ip=new IPv4(ipString);
		}catch(Exception e){
		}
		return ip;
	}
}
