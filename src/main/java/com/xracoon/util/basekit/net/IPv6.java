package com.xracoon.util.basekit.net;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.xracoon.util.basekit.StringEx;

public class IPv6 extends IP {
	public static final String localhost="0:0:0:0:0:0:0:1"; //冒分十六进制, 共128b, 分8段, 每段16b(4个16进制数)。 开头的0可省略。 0位压缩表示法。内嵌IPv4
	private int[] section = new int[8];
	
	public IPv6(){};
	public IPv6(String ip) throws IllegalIPv6AddressException{
		parse(ip);
	}
	@Override
	public byte getVersion() {
		return IPv6;
	}
	@Override
	public IPv6 parse(String ip) throws IllegalIPv6AddressException {
		if (StringEx.isBlank(ip))
			throw new IllegalIPv6AddressException(ip);
		
		ip= extend(ip);
		
		String[] parts = ip.split(":");
		if(parts.length!=8)
			throw new IllegalIPv6AddressException(ip);
		
		for (int i = 0; i < parts.length; i++) {
			String s = parts[i];
			if (!s.matches("[1-9a-fA-F0]{1,4}"))
				throw new IllegalIPv6AddressException(ip);
			int n = -1;
			try {
				n = Integer.parseInt(s,16);
			} catch (Exception e) {
				throw new IllegalIPv6AddressException(ip);
			}
			if (n < 0 || n > 0xFFFF)
				throw new IllegalIPv6AddressException(ip);
			section[i]=(short) n;
		}
		return this;
	}
	
	public static int count(String string, String serachText){
		int i=-1,n=0;
		while((i=string.indexOf(serachText,i+1))>-1)
			n++;
		return n;
	}
	
	public static String extend(String ipString){
		if(!ipString.contains("::"))
			return ipString;
		
		if(ipString.endsWith("::"))
			ipString+="0";
		
		String[] parts= ipString.split("::");
		if(parts.length!=2)
			return "";

		int n= StringEx.isBlank(parts[0])?0:count(parts[0],":")+1;
		n+= count(parts[1],":")+1;
		StringBuilder sb=new StringBuilder();
		sb.append(StringEx.isBlank(parts[0])?"":parts[0]+":");
		for(int i=8-n; i>0; i--)
			sb.append("0:");
		sb.append(parts[1]);
		return sb.toString();
	}
	
	private static final Pattern collapsePattern= Pattern.compile("(?<=:|\\b)0(:0)+(?=:|\\b)");
	public static String collapse(String input){
		if(StringEx.isBlank(input)||!input.contains(":"))
			return input;

		Matcher m= collapsePattern.matcher(input);
		int start=-1;
		int end=-1;
		int maxlen= 0;
		while(m.find()){
			if(m.end()-m.start()>maxlen){
				start=m.start();
				end=m.end();
				maxlen= end- start;
			}
		}
		if(start<0 && end <0)
			return input;
		
		StringBuilder builder=new StringBuilder();
		builder.append(input.substring(0,start));
		if(end<input.length())
			builder.append(input.substring(end));
	
		if(builder.length()==0) //0:0:0:0:0:0:0:0
			builder.append("::");
		else if(builder.charAt(0)==':')
			builder.insert(0, ':');
		else if(builder.charAt(builder.length()-1)==':')
			builder.append(':');
		
		return builder.toString();
	}
	
	public static class IllegalIPv6AddressException extends IllegalIPAddressException{
		private static final long serialVersionUID = 1042358503401177918L;
		public IllegalIPv6AddressException(String message){
			super(message);
		}
		public IllegalIPv6AddressException(String message, Throwable cause) {
			super(message, cause);
		}
	}
	
	@Override
	public String toString(){
		return String.format("%x:%x:%x:%x:%x:%x:%x:%x", 
				section[0],section[1],section[2],section[3],
				section[4],section[5],section[6],section[7]);
	}
	@Override
	public boolean equals(Object obj) {
		if(obj==this)
			return true;
		if(obj instanceof IPv6)
			return Arrays.equals(((IPv6)obj).section, section);
		return false;
	}
	@Override
	public int hashCode(){
		return toString().hashCode();
	}
	@Override
	public boolean isPrivateAddress() {
		return false;
	}
}
