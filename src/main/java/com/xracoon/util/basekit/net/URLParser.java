package com.xracoon.util.basekit.net;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

public class URLParser {
	
	private String urlString;
	private String baseUrl;
	private String queryString;
	private Map<String,String[]> params;
	
	public URLParser(String urlString){
		parse(urlString,"UTF-8");
	}

	public URLParser(String urlString, String enc){
		parse(urlString,enc);
	}
	
	public void parse(String urlString, String enc) {
		if (urlString == null || urlString.length() == 0)
			return;

		this.urlString=urlString;
		
		int questIndex = urlString.indexOf('?');
		if (questIndex == -1)
			baseUrl=urlString;
		else
		{
			baseUrl = urlString.substring(0, questIndex);
			queryString = urlString.substring(questIndex + 1, urlString.length());
		}
		
		//---params--
		params = new HashMap<String, String[]>();
		if (queryString != null && queryString.length() > 0) {
			int ampersandIndex, lastAmpersandIndex = 0;
			String subStr, param, value;
			String[] paramPair, values, newValues;
			do {
				ampersandIndex = queryString.indexOf('&', lastAmpersandIndex) + 1;
				if (ampersandIndex > 0) {
					subStr = queryString.substring(lastAmpersandIndex,
							ampersandIndex - 1);
					lastAmpersandIndex = ampersandIndex;
				} else {
					subStr = queryString.substring(lastAmpersandIndex);
				}
				paramPair = subStr.split("=");
				param = paramPair[0];
				value = paramPair.length == 1 ? "" : paramPair[1];
				try {
					value = URLDecoder.decode(value, enc);
				} catch (UnsupportedEncodingException ignored) {
				}
				if (params.containsKey(param)) {
					values = params.get(param);
					int len = values.length;
					newValues = new String[len + 1];
					System.arraycopy(values, 0, newValues, 0, len);
					newValues[len] = value;
				} else {
					newValues = new String[] { value };
				}
				params.put(param, newValues);
			} while (ampersandIndex > 0);
		}
	}


	public String getUrlString() {
		return urlString;
	}
	public String getBaseUrl() {
		return baseUrl;
	}
	public String getQueryString() {
		return queryString;
	}
	public Map<String, String[]> getParams() {
		return params;
	}

	
}
