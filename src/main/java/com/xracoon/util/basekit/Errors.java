package com.xracoon.util.basekit;

public class Errors {
	public static String getMessage(Throwable ex){
		String msg= ex.getMessage();
		if(StringEx.isBlank(msg))
			msg= ex.getClass().getName();
		return msg;
	}
}
