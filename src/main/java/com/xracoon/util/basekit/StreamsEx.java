/**
* @author yangtianxin
* @date 2015-10-15
* Copyright 2015, .... All right reserved. 
*/

package com.xracoon.util.basekit;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class StreamsEx {
	public static int BUFFSIZE=4096;
	
	static public void copy(InputStream is, OutputStream os) throws IOException{
		byte[] buf=new byte[BUFFSIZE];
		int len=0;
		while((len=is.read(buf))!=-1){
            os.write(buf,0,len);
        }
		os.flush();
	}
	
	static public void copy(InputStream is, OutputStream os, boolean closeAfterCopy) throws IOException{
		byte[] buf=new byte[BUFFSIZE];
		int len=0;
		while((len=is.read(buf))!=-1){
            os.write(buf,0,len);
        }
		os.flush();
		if(closeAfterCopy){
			is.close();
			os.close();
		}
	}
	
	static public byte[] read(InputStream is) throws IOException{
		ByteArrayOutputStream bo=new ByteArrayOutputStream();
		copy(is,bo);
		return bo.toByteArray();
	}
	
	static public void write(OutputStream os, byte[] bytes) throws IOException{
		ByteArrayInputStream bi=new ByteArrayInputStream(bytes);
		copy(bi,os);
		os.flush();
	}
}
