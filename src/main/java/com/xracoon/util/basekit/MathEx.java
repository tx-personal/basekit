package com.xracoon.util.basekit;

public class MathEx {
	public static double round(double d, int pow){
		if(pow<=0)
			pow=2;
		
		double power=Math.pow(10, pow);
		return (int)(d*power+0.5)/power;
	}
}
