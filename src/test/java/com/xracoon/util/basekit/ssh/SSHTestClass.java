package com.xracoon.util.basekit.ssh;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;

import org.junit.Ignore;
import org.junit.Test;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.xracoon.util.basekit.FilesEx;
import com.xracoon.util.basekit.security.auth.Credential;
import com.xracoon.util.basekit.ssh.JschSSHUtil;

public class SSHTestClass {
	private String ip="111.111.111.111";
	private int port=22;
	private String user="111";
	private String pass="111";
	private String privkeyfile="xxxxx";
		
	@Test
	@Ignore
	public void testSshdExec() throws Exception {
		SshdSSHUtil sshtool=new SshdSSHUtil(user, ip, port, privkeyfile);
		System.out.println(sshtool.remoteExecute("ifconfig"));
		System.out.println(sshtool.remoteExecute("ls"));
	}

	@Test
	@Ignore
	public void testSshdWrite() throws IOException, GeneralSecurityException {
		SshdSSHUtil sshtool=new SshdSSHUtil(user, ip, port, privkeyfile);
		sshtool.writeFile(new ByteArrayInputStream("测试字符串".getBytes()), "sshtemp/test1/writeTest.txt");
	}
	
	@Test
	@Ignore
	public void testSshdRead() throws IOException, GeneralSecurityException {
		SshdSSHUtil sshtool=new SshdSSHUtil(ip,port, user, pass.toCharArray());
		
		ByteArrayOutputStream os=new ByteArrayOutputStream();
		sshtool.readFile("sshtemp/test1/writeTest.txt", os);
		System.out.println(new String(os.toByteArray()));
	}
	
	@Test
	@Ignore
	public void testJschExec() throws Exception {
		JschSSHUtil sshtool=new JschSSHUtil(ip, port, Credential.SecretKeyPem(user, FilesEx.readString(privkeyfile), pass.toCharArray()));
		System.out.println(sshtool.remoteExecute("ifconfig"));
		System.out.println(sshtool.remoteExecute("ls"));
	}
	
	@Test
	@Ignore
	public void testJschWrite() throws IOException, GeneralSecurityException, JSchException, SftpException, InterruptedException {
		JschSSHUtil sshtool=new JschSSHUtil(ip,port,user, pass.toCharArray());
		
		sshtool.writeFile(new ByteArrayInputStream("我是中国人".getBytes()), "sshtemp/writeTest.txt");
	}
	
	@Test
	@Ignore
	public void testJschRead() throws IOException, GeneralSecurityException, JSchException, SftpException {
		JschSSHUtil sshtool=new JschSSHUtil(ip,port,user, pass.toCharArray());
		
		ByteArrayOutputStream os=new ByteArrayOutputStream();
		sshtool.readFile("sshtemp/writeTest.txt", os);
		System.out.println(new String(os.toByteArray()));
	}
}
