package com.xracoon.util.basekit;

import org.junit.Test;

public class AssertsExTestCase {
	
	@Test(expected = AssertionError.class)
	public void testDiffBiggerThanPrecious(){
		AssertsEx.assertEqualReal(100, 100.123, 0.001);
	}
	
	@Test(expected = AssertionError.class)
	public void testDiffEqualToPrecious(){
		AssertsEx.assertEqualReal(100, 100.001, 0.001);
	}
	
	@Test
	public void testDiffLessThanPrecious(){
		AssertsEx.assertEqualReal(100, 100.0009, 0.001);
	}
}
