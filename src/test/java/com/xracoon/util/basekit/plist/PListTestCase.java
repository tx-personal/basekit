package com.xracoon.util.basekit.plist;
import java.io.IOException;
import java.io.StringWriter;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.text.ParseException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.bouncycastle.cms.CMSException;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import com.xracoon.util.basekit.FilesEx;
import com.xracoon.util.basekit.plist.PlistUtil;
import com.xracoon.util.basekit.plist.PropertyList;
import com.sun.org.apache.xml.internal.serialize.*;

public class PListTestCase {

	@SuppressWarnings("restriction")
	@Test
	public void testLoad() throws IOException, CMSException, ParserConfigurationException, SAXException, ParseException, TransformerException, CertificateException, NoSuchAlgorithmException {

		PlistUtil parser=new PlistUtil();
		PropertyList plist=parser.parse(FilesEx.readString("test.plist"));
		String a=plist.getData("CFBundleShortVersionString").getValue().toString();
		
		
		Document doc=parser.format(plist);
        StringWriter buffer=new StringWriter();
        
        //Intent bug
//		TransformerFactory tff = TransformerFactory.newInstance();
//        Transformer tf = tff.newTransformer();
//        tf.setOutputProperty(OutputKeys.INDENT, "yes"); 
//        tff.setAttribute("indent-number", new Integer(2));  
//        tf.transform(new DOMSource(doc), new StreamResult(buffer));
        
        //Only for Sun JRE
        OutputFormat format =new OutputFormat(doc); 
        format.setDoctype(plist.getPublicId(), plist.getSystemId());
        format.setIndenting(true);  
        format.setIndent(2);  
        XMLSerializer serializer = new XMLSerializer(buffer, format);  
        serializer.serialize(doc);  
        
       System.out.println(buffer.toString());
	}

}
