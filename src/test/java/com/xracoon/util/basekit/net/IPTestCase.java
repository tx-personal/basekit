package com.xracoon.util.basekit.net;

import static org.junit.Assert.*;
import org.junit.Test;
import com.xracoon.util.basekit.net.IPv4.IllegalIPv4AddressException;
import com.xracoon.util.basekit.net.IPv6.IllegalIPv6AddressException;

public class IPTestCase {
	@Test
	public void testIPv4Parse() throws IllegalIPv4AddressException{
		System.out.println(new IPv4().parse("1.2.3.255").toString());
	}
	@Test(expected= IllegalIPv4AddressException.class)
	public void testInvaildIPv4_1() throws IllegalIPv4AddressException{
		System.out.println(new IPv4().parse("1.2.3.256").toString());
	}
	@Test(expected= IllegalIPv4AddressException.class)
	public void testInvaildIPv4_2() throws IllegalIPv4AddressException{
		System.out.println(new IPv4().parse("adfb").toString());
	}
	@Test(expected= IllegalIPv4AddressException.class)
	public void testInvaildIPv4_3() throws IllegalIPv4AddressException{
		System.out.println(new IPv4().parse("1.2.3.4.5.6").toString());
	}
	@Test(expected= IllegalIPv4AddressException.class)
	public void testInvaildIPv4_4() throws IllegalIPv4AddressException{
		System.out.println(new IPv4().parse("1.2.3").toString());
	}
	

	
	@Test
	public void testIPv6Parse() throws IllegalIPv6AddressException{
		System.out.println(new IPv6().parse("::").toString());
		System.out.println(new IPv6().parse("::1").toString());
		System.out.println(new IPv6().parse("::2:3").toString());
		System.out.println(new IPv6().parse("1::2").toString());
		System.out.println(new IPv6().parse("1:2::3:4").toString());
		System.out.println(new IPv6().parse("1:2:3:4:5:DD:EE:FFFF").toString());
	}
	
	@Test
	public void testIPv6Collapse(){
		assertEquals("10::1:0:0:01", IPv6.collapse("10:0:0:0:1:0:0:01"));
		assertEquals("::1:0:0:1", IPv6.collapse("0:0:0:0:1:0:0:1"));
		assertEquals("::1:1:0:0:0", IPv6.collapse("0:0:0:1:1:0:0:0"));
		assertEquals("::", IPv6.collapse("0:0:0:0:0:0:0:0"));
		assertEquals("::1", IPv6.collapse("0:0:0:0:0:0:0:1"));
		assertEquals("1::", IPv6.collapse("1:0:0:0:0:0:0:0"));
		assertEquals("0:1:1:0:1:1:1:0", IPv6.collapse("0:1:1:0:1:1:1:0"));
	}

	@Test
	public void testIPv6Expend(){
		assertEquals("10:0:0:0:1:0:0:01", IPv6.extend("10::1:0:0:01"));
		assertEquals("0:0:0:0:1:0:0:1", IPv6.extend("::1:0:0:1"));
		assertEquals("0:0:0:1:1:0:0:0", IPv6.extend("::1:1:0:0:0"));
		assertEquals("0:0:0:0:0:0:0:0", IPv6.extend("::"));
		assertEquals("0:0:0:0:0:0:0:1", IPv6.extend("::1"));
		assertEquals("1:0:0:0:0:0:0:0", IPv6.extend("1::"));
		assertEquals("0:1:1:0:1:1:1:0", IPv6.extend("0:1:1:0:1:1:1:0"));
	}
	
	
	@Test(expected= IllegalIPv6AddressException.class)
	public void testInvaildIPv6_1() throws IllegalIPv6AddressException{
		System.out.println(new IPv6().parse("asdfasdf").toString());
	}
	@Test(expected= IllegalIPv6AddressException.class)
	public void testInvaildIPv6_2() throws IllegalIPv6AddressException{
		System.out.println(new IPv6().parse("1.2.3.4").toString());
	}
	@Test(expected= IllegalIPv6AddressException.class)
	public void testInvaildIPv6_3() throws IllegalIPv6AddressException{
		System.out.println(new IPv6().parse("1:2::3:1FFFF").toString());
	}
	@Test(expected= IllegalIPv6AddressException.class)
	public void testInvaildIPv6_4() throws IllegalIPv6AddressException{
		System.out.println(new IPv6().parse("1:2:3:4:5:6:7:8:9").toString());
	}
	@Test(expected= IllegalIPv6AddressException.class)
	public void testInvaildIPv6_5() throws IllegalIPv6AddressException{
		System.out.println(new IPv6().parse("1::2::3:FFFF").toString());
	}
	@Test(expected= IllegalIPv6AddressException.class)
	public void testInvaildIPv6_6() throws IllegalIPv6AddressException{
		System.out.println(new IPv6().parse("1:2:3").toString());
	}
}
