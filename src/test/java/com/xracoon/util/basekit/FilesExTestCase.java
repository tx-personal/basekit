package com.xracoon.util.basekit;

import static org.junit.Assert.*;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import org.junit.Test;

public class FilesExTestCase {
	
	@Test
	public void testReadNormalFile() throws Exception{
		URL u=this.getClass().getResource("/test.txt");
		String file=u.getFile();
		if(System.getProperties().getProperty("os.name").toUpperCase().indexOf("WINDOWS") != -1)
			file=StringEx.trimLeft(file,'/');
		
		String[] lines=FilesEx.readString(file).split("[\r\n]+");
		assertEquals("test title",lines[0]);
		assertEquals("test content.",lines[1]);
	}
	
	@Test
	public void testReadFromClassPath() throws Exception{
		String[] lines=FilesEx.readString("classpath:test.txt").split("[\r\n]+");
		assertEquals("test title",lines[0]);
		assertEquals("test content.",lines[1]);
	}
	
	@Test
	public void testReadFromClassPathImplicit() throws Exception{
		String[] lines=FilesEx.readString("test.txt").split("[\r\n]+");
		assertEquals("test title",lines[0]);
		assertEquals("test content.",lines[1]);
	}
	
	@Test
	public void testReadFrom() throws Exception{
		String content=FilesEx.readString("http://news.baidu.com/");
		assertTrue(content.contains("百度"));
	}
	
	@Test
	public void testDeleteTree() throws Exception{
		Path root=Files.createTempDirectory("a");
        File file1=new File(root.toFile(),"sub1/subsub1");
        file1.getParentFile().mkdirs();
        FilesEx.writeFile("test content", file1);
        File file2=new File(root.toFile(),"sub2/subsub2");
        file2.getParentFile().mkdirs();
        FilesEx.writeFile("test content", file2);
        
        assertTrue(root.toFile().list().length==2);
        FilesEx.deleteTree(root.toFile());
        assertTrue(!root.toFile().exists());
	}
	
	@Test
	public void testGetBriefSize() throws Exception{
        assertEquals(FilesEx.getBriefSize(123),"123B");
        assertEquals(FilesEx.getBriefSize((long)(1.24*1024)),"1.2K");
        assertEquals(FilesEx.getBriefSize((long)(1.25*1024)),"1.3K");
        assertEquals(FilesEx.getBriefSize((long)(1.24*1024*1024)),"1.2M");
        assertEquals(FilesEx.getBriefSize((long)(1.25*1024*1024)),"1.3M");
        assertEquals(FilesEx.getBriefSize((long)(1.24*1024*1024*1024)),"1.2G");
        assertEquals(FilesEx.getBriefSize((long)(1.25*1024*1024*1024)),"1.3G");
	}
	
	@Test 
	public void testZip() throws URISyntaxException, IOException{
		File basepath= FilesEx.getFile("classpath:ziptest/4zip");
		String[] files= new String[]{"aaa.txt","afolder/bbb.txt"};
		File zipFile= new File(basepath,"../4zip.zip");
		if(zipFile.exists())
			zipFile.delete();
		FilesEx.zip(basepath, files, zipFile);
		assertTrue(zipFile.exists());
		
		File output= new File(basepath,"../unzip");
		if(output.exists())
			FilesEx.deleteTree(output);
		FilesEx.unzip(zipFile, output);
		
		assertEquals(FilesEx.readString(new File(output, "afolder/bbb.txt").getAbsolutePath()),
				FilesEx.readString(new File(basepath,"afolder/bbb.txt").getAbsolutePath()));
		assertEquals(FilesEx.readString(new File(output, "aaa.txt").getAbsolutePath()),
				FilesEx.readString(new File(basepath,"aaa.txt").getAbsolutePath()));
		assertTrue(!new File(output, "ccc.txt").exists());
	}
	
	@Test
	public void testGetFile() throws IOException, URISyntaxException{
		File f1= FilesEx.getFile("classpath:ziptest/4zip/aaa.txt");
		File f2= FilesEx.getFile(f1.getAbsolutePath());
		assertEquals("aaaa", FilesEx.readString(f1.getAbsolutePath()));
		assertEquals("aaaa", FilesEx.readString(f2.getAbsolutePath()));
	}
}
