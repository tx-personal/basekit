package com.xracoon.util.basekit.system;

import static org.junit.Assert.*;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.LoggerFactory;
import com.xracoon.util.basekit.hudson.proc.Proc;
import com.xracoon.util.basekit.system.OS.ExecArg;

public class OSTestCase {

	@Test
	public void testExecScriptSyn() throws IOException {
		OS os=OS.getSingleton();
		String str=os.execScriptSyn("echo %PATH%\r\necho %JAVA_HOME%", null, new File("E:/wsJava/basekit")).getLog();
		assertTrue(str.contains(":"));
	}
	
	@Test
	@Ignore
	public void testRun1() throws Exception{
		OS os=OS.getNewInstance();
		String[] cmd=new String[]{"cmd.exe","/c","call", "F:/Temp/startcmd.bat"};
		ExecStatus es=os.execSyn(cmd,null,null);
		System.out.println(es.getRetVal());
	}
	
	@Test
	@Ignore
	public void testExecArgs(){
		ExecArg ea=ExecArg.Build().Cmdline("git clone aaa \"asd s'df\"     123");
		ea.Pwd(new File("."));
	} 
	
	@Test
	@Ignore
	public void testKill() throws IOException, InterruptedException{
		String[] cmds=new String[]{"cmd.exe","/c","call", "F:/Temp/startcmd.bat"};
		final Proc p =new Proc.LocalProc(cmds, null, System.in, System.out, System.err, null, LoggerFactory.getLogger(OSTestCase.class), true);
		
		final Timer timer=new Timer(false);
		timer.schedule(new TimerTask(){
			@Override
			public void run() {
				try {
					p.kill();
					System.out.println("kill....");
				} catch (Exception e) {
					e.printStackTrace();
				}
				timer.cancel();
			}
		}, 5000, 5000);
		
		while(p.isAlive()){
			Thread.sleep(1000);
		}
		System.out.println("Done");
	}
	
	@Test
	@Ignore
	public void testAppendHost() throws IOException{
//		File file= new File("C:\\WINDOWS\\System32\\drivers\\etc\\hosts");
//		boolean ret=FilesEx.deleteTree(file);
//		System.out.println(ret);
		
		OS os=OS.getSingleton();
		os.backupHost("host.bak");
		Map<String,String> hostIpMap=new LinkedHashMap<>();
		hostIpMap.put("www.aaaa.com www.bbb.com", "127.0.0.1");
		hostIpMap.put("www.ccc.com", "127.0.0.2");
		OS.getSingleton().appendHost(hostIpMap);
		
		assertEquals(InetAddress.getByName("www.ccc.com").getHostAddress(),"127.0.0.2");
		os.resotreHost("host.bak");
	}
}
