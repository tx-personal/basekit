package com.xracoon.util.basekit;

import java.security.Provider;
import java.security.Security;
import java.util.Map.Entry;

import org.junit.Test;

public class ProviderTestCase {

	@Test
	public void listProviders(){
		Provider[] providers = Security.getProviders();
		for(Provider p: providers){
		    System.out.println(p.getName()+"_"+p.getInfo()+"_"+p.getVersion());
		    for(Entry<Object,Object> e: p.entrySet()){
		        System.out.println(e.getKey()+" = "+ e.getValue());
		    }
		}
	}
}
