package com.xracoon.util.basekit.db;

import static org.junit.Assert.*;
import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.Test;
import com.xracoon.util.basekit.FilesEx;

public class DitUtilTestCase {

	@Test
	public void testGeneEnum() throws Exception {
		String packName="com.xracoon.basekit.testoutput";
		String className="Dict";
		
		Map<String, Map<String, Integer>> map = new LinkedHashMap<>();
		Map<String, Integer> fruit = new LinkedHashMap<>();
		fruit.put("Apple", 1);
		fruit.put("Pear", 2);
		map.put("Fruit", fruit);
		Map<String, Integer> meat = new LinkedHashMap<>();
		meat.put("Beef", 100);
		meat.put("Fish", 101);
		map.put("Meat", meat);

		String output = DictUtil.generate(map, packName, className);
		System.out.println(output);
		String expect=FilesEx.readString("classpath:DataEnums.java.txt");
		assertEquals(output.trim().replaceAll("[\r\n]+", ""),expect.trim().replaceAll("[\r\n]+", ""));
	}

}
