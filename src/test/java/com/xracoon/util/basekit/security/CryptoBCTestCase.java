package com.xracoon.util.basekit.security;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import org.junit.Test;
import com.xracoon.util.basekit.FilesEx;

public class CryptoBCTestCase {
	@Test
	public void testLoadPemKeyPair() throws Exception{
		KeyPair privateKey=CryptoBC.readKeyPairPEM(FilesEx.readString("pem_prv"), null);
		//PrivateKey privateKey=Crypto.readPEMPriKey("RSA", "testloadfile_priv");
		assertTrue(privateKey!=null);
		
		//PublicKey publicKey=Crypto.readPEMPubKey("RSA", "testloadfile_pub");
		//assertTrue(publicKey!=null);
	}
	
	@Test
	public void testLoadPemPubkey() throws IOException{
		PublicKey pubKey = CryptoBC.readPubkeyPEM(FilesEx.readString("pem_pub"));
		assertTrue(pubKey!=null);
	}
	
	@Test
	public void testLoadRsaPubkeySSH() throws InvalidKeySpecException, NoSuchAlgorithmException, IOException{
		PublicKey sshpubkey=Crypto.readPubKeySSH(FilesEx.readString("ssh_pub"));
		PublicKey pempubkey = CryptoBC.readPubkeyPEM(FilesEx.readString("pem_pub"));
		assertTrue(Arrays.equals(sshpubkey.getEncoded(),pempubkey.getEncoded()));
	}
	
	@Test
	public void testLoadDsaPubkeySSH() throws InvalidKeySpecException, NoSuchAlgorithmException, IOException{
		KeyPair kp = CryptoBC.readKeyPairPEM(FilesEx.readString("dsa_prv"),null);
		PublicKey sshpubkey=Crypto.readPubKeySSH(FilesEx.readString("ssh_dsa_pub"));
		assertTrue(Arrays.equals(sshpubkey.getEncoded(),kp.getPublic().getEncoded()));
	}
}
