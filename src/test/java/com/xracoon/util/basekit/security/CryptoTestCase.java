package com.xracoon.util.basekit.security;

import static org.junit.Assert.*;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import org.junit.Test;
import com.xracoon.util.basekit.FilesEx;

public class CryptoTestCase {
	String text="测试文本, test text";
	
	@Test
	public void testDES() throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException {
		SecretKey key=Crypto.newSecretKey("DESede",null,null);
		byte[] keybytes=key.getEncoded();
		byte[] encodedtext=Crypto.encrypt(key, text.getBytes());
		byte[] keybase64Enc=Codec.encBase64CC(keybytes);
		byte[] keybase64Dec=Codec.decBase64CC(keybase64Enc);
		
		System.out.println("key: "+new String(keybase64Enc));
		System.out.println("encoded: "+new String(encodedtext));
		
		SecretKey key1=Crypto.loadSecretKey(keybase64Dec, "DESede");
		String text1=new String(Crypto.decrypt(key1, encodedtext));
		System.out.println("revert: "+text1);
		assertEquals(text, text1);
	}
	
	@Test
	public void testAES() throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException {
		SecretKey key=Crypto.newSecretKey("AES",null,null);
		byte[] keybytes=key.getEncoded();
		byte[] encodedtext=Crypto.encrypt(key, text.getBytes());
		byte[] keybase64Enc=Codec.encBase64CC(keybytes);
		byte[] keybase64Dec=Codec.decBase64CC(keybase64Enc);
		
		System.out.println("key: "+new String(keybase64Enc));
		System.out.println("encoded: "+new String(encodedtext));
		
		SecretKey key1=Crypto.loadSecretKey(keybase64Dec, "AES");
		String text1=new String(Crypto.decrypt(key1, encodedtext));
		System.out.println("revert: "+text1);
		assertEquals(text, text1);
	}
	
	@Test
	public void testRsaDesCombind() throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException
	{	
		String key="VCP7FmFeaLpkKsjml6RYerai1mEf+/GF";
		KeyPair keypair=Crypto.newKeyPair("RSA",null,null);
		keypair.getPrivate();
		PublicKey publicKey= keypair.getPublic();
		PrivateKey privateKey=keypair.getPrivate();
		
		String prvkeystr=new String(Codec.encBase64CC(privateKey.getEncoded()));
		String pubkeystr=new String(Codec.encBase64CC(publicKey.getEncoded()));
		System.out.println("prvkey: \n"+prvkeystr);
		System.out.println("pubkey: \n"+pubkeystr);

		privateKey=Crypto.readPrikeyDER("RSA", Codec.decBase64CC(prvkeystr.getBytes()));
		publicKey=Crypto.readPubkeyDER("RSA", Codec.decBase64CC(pubkeystr.getBytes()));
		
		byte[] bytes=Crypto.encrypt(publicKey, Codec.decBase64CC(key.getBytes()));
		System.out.println("encrypted: "+new String(Codec.encBase64CC(bytes)));
		byte[] decryptBytes=Crypto.decrypt(privateKey, bytes);
		System.out.println("decrypted: "+new String(Codec.encBase64CC(decryptBytes)));
		System.out.println("origion  : "+key);
		
		SecretKey secretKey=Crypto.loadSecretKey(decryptBytes, "DESede");
		byte[] secretcontent=Crypto.encrypt(secretKey, text.getBytes());
		byte[] restorecontent=Crypto.decrypt(secretKey, secretcontent);
		System.out.println("origin  text: "+text);
		System.out.println("resotre text: "+new String(restorecontent));
		
		assertEquals(text, new String(restorecontent));
	}
	
	
	@Test
	public void testLoadKeys() throws Exception{	
		PrivateKey privateKey=Crypto.readPrikeyDER("RSA", Codec.decBase64(FilesEx.readString("base64_der_prv").getBytes()));
		assertTrue(privateKey!=null);
		
		//PrivateKey privateKey1=Crypto.readPrikeyDER("RSA", Codec.decBase64(Crypto.trimDesc(FilesEx.readLines("base64_der_prv2_with_desc")).getBytes()));
		//assertTrue(privateKey1!=null);
		
		PublicKey publicKey=Crypto.readPubkeyDER("RSA", Codec.decBase64(FilesEx.readString("base64_der_pub").getBytes()));
		assertTrue(publicKey!=null);
	}
	
	@Test
	public void testGetRsaPubkeyFromPrivkey(){
		KeyPair pair=Crypto.newKeyPair("RSA", 2048, null);
		PublicKey pubKey=Crypto.getPubkey(pair.getPrivate());
		assertTrue(Arrays.equals(pubKey.getEncoded(),pair.getPublic().getEncoded()));
		//System.out.println(pair.getPublic().toString());
		//System.out.println(pubKey.toString());
		System.out.println(new String(Codec.encBase64(pair.getPublic().getEncoded())));
		System.out.println(new String(Codec.encBase64(pair.getPrivate().getEncoded())));
		
		pair=Crypto.newKeyPair("DSA", 2048, null);
		pubKey=Crypto.getPubkey(pair.getPrivate());
		assertTrue(Arrays.equals(pubKey.getEncoded(),pair.getPublic().getEncoded()));
		//System.out.println(pair.getPublic().toString());
		//System.out.println(pubKey.toString());
	}
}
