package com.xracoon.util.basekit;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;

public class StringExTestCase {
	@Test
	public void testTrimLeft1(){
		assertEquals("asdf//",StringEx.trimLeft("///asdf//", '/'));
	}
	@Test
	public void testTrimLeft2(){
		assertEquals("",StringEx.trimLeft("/////", '/'));
	}
	@Test
	public void testTrimRight1(){
		assertEquals("///asdf",StringEx.trimRight("///asdf//", '/'));
	}
	@Test
	public void testTrimRight2(){
		assertEquals("",StringEx.trimRight("/////", '/'));
	}
	@Test
	public void testTrim1(){
		assertEquals("asdf",StringEx.trim("///asdf//", '/'));
	}
	@Test
	public void testTrim2(){
		assertEquals("",StringEx.trim("/////", '/'));
	}
	@Test
	public void testHex(){
		byte[] bytes="了空间撒地方".getBytes();
		String hexStr=StringEx.bytes2Hex(bytes);
		System.out.println(hexStr);
		byte[] revert=StringEx.hex2Bytes(hexStr);
		assertEquals(new String(bytes),new String(revert));
	}
	
	@Test
	public void testRevese(){
		String s="I am a 中国人。";
		assertEquals("。人国中 a ma I", StringEx.reverse(s));
	}
	@Test
	public void testIsNumber(){
		assertEquals(true, StringEx.isNumber("123124"));
		assertEquals(false, StringEx.isNumber("asasdf"));
		assertEquals(false, StringEx.isNumber("123123asasdf"));
		assertEquals(true, StringEx.isNumber("12.3124"));
		assertEquals(false, StringEx.isNumber("123124."));
		assertEquals(false, StringEx.isNumber(".123124"));
		assertEquals(false, StringEx.isNumber("123爱离开的叫法"));
		assertEquals(false, StringEx.isNumber(null));
		assertEquals(false, StringEx.isNumber(" \n "));
		assertEquals(false, StringEx.isNumber(""));
	}
	@Test
	public void testIsHex(){
		assertEquals(true, StringEx.isHex("aceFAdc1234"));
		assertEquals(false, StringEx.isHex("2123abdHkdf"));
		assertEquals(true, StringEx.isHex("1A2.3e124"));
		assertEquals(false, StringEx.isHex("123A124."));
		assertEquals(false, StringEx.isHex(".1231F24"));
		assertEquals(false, StringEx.isHex("12案例的咖啡机"));
		assertEquals(false, StringEx.isHex(null));
		assertEquals(false, StringEx.isHex(" \n "));
		assertEquals(false, StringEx.isHex(""));
	}
	@Test
	public void testResolverToken(){
		String a=" dfasdf${ t.1 } fdasdf$T2 fdsd$T.1 $t2 lks$t2jdf $t3 ${t3}";
		Map<String, String> map=ArraysEx.toMap("t.1","1111","t2", "2222");
		String b=StringEx.resolveToken(a, map);
		
		assertEquals(b, " dfasdf1111 fdasdf2222 fdsd1111 2222 lks$t2jdf $t3 ${t3}");
	}
	@Test
	public void testResolverTokenEscape(){
		String a="DEL /Q ${ WORKSPACe }\\.git $ccc $aaa 123";
		Map<String, String> map=ArraysEx.toMap("WORKSPACE","E:\\aaa\\bbb","ccc","ttt");
		String b=StringEx.resolveToken(a, map);
		
		assertEquals("DEL /Q E:\\aaa\\bbb\\.git ttt $aaa 123", b);
	}
	@Test
	public void testDateRangeDesc(){
		assertEquals(StringEx.dateRangeDesc(500),"500ms");
		assertEquals(StringEx.dateRangeDesc(2500),"2s 500ms");
		assertEquals(StringEx.dateRangeDesc(1000*60*3+1000*30),"3m 30s");
		assertEquals(StringEx.dateRangeDesc(1000*60*60*5+1000*60*3+1000*30),"5h 3m 30s");
		assertEquals(StringEx.dateRangeDesc(1000*60*60*16*24+1000*60*60*5+1000*60*3+1000*30),"16d 5h 3m 30s");
	}
}
