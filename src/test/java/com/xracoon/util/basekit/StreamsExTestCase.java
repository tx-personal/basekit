package com.xracoon.util.basekit;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.junit.Test;

public class StreamsExTestCase {
	
	@Test
	public void testCopy() throws Exception{
		StreamsEx.BUFFSIZE=32;
		byte[] bytes="adkfja4654654654546546545646lksdjflajsdfasdfasdfasdf".getBytes();
		ByteArrayInputStream is=new ByteArrayInputStream(bytes);
		ByteArrayOutputStream os=new ByteArrayOutputStream();
		StreamsEx.copy(is, os);
		
		assertEquals(new String(os.toByteArray()), new String(bytes));
	}
	
	@Test
	public void testWrite() throws Exception{
		StreamsEx.BUFFSIZE=32;
		byte[] bytes="adkfja4654654654546546545646lksdjflajsdfasdfasdfasdf".getBytes();
		ByteArrayOutputStream os=new ByteArrayOutputStream();
		StreamsEx.write(os, bytes);;
		
		assertEquals(new String(os.toByteArray()), new String(bytes));
	}
	
	@Test
	public void testRead() throws Exception{
		StreamsEx.BUFFSIZE=32;
		byte[] bytes="adkfja4654654654546546545646lksdjflajsdfasdfasdfasdf".getBytes();
		ByteArrayInputStream is=new ByteArrayInputStream(bytes);
		byte[] bytes2=StreamsEx.read(is);
		
		assertEquals(new String(bytes), new String(bytes2));
	}
}
