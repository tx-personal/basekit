package com.xracoon.util.basekit;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class ErrorsTestCase {
	@Test
	public void testGetMessage() {
		String msg="----";
		try{ throw new Exception("111");
		}catch(Exception e){ msg= Errors.getMessage(e);}
		assertEquals(msg, "111");
		
		try{ throw new IOException();
		}catch(Exception e){ msg= Errors.getMessage(e);}
		assertEquals(msg, "java.io.IOException");
	}
}
