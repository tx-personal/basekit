package com.xracoon.util.basekit;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;

public class ArraysExTestCase {
	
	@Test
	public void testConcat() throws Exception{
		String[] a1={"AAA","BBB","CCC"};
		assertEquals(ArraysEx.join(a1, "-"),"AAA-BBB-CCC");
		assertEquals(ArraysEx.join(a1, null),"AAA,BBB,CCC");
		
		Integer[] b1={1,2,3};
		assertEquals(ArraysEx.join(b1, "0"),"10203");
		
		assertEquals(ArraysEx.join(new String[]{"AAA"}, "-"),"AAA");
		
		assertEquals(ArraysEx.join(new String[]{}, "-"),"");
		
		assertEquals(ArraysEx.join(null, "-"),null);
	}
	
	@Test
	public void testToMap(){
		Map<String, String> map=ArraysEx.toMap("t.1","1111","t2", "2222","t3");
		assertEquals(map.size(), 2);
		assertEquals(map.get("t.1"), "1111");
		assertEquals(map.get("t2"), "2222");
	}
	
}
