package com.xracoon.util.basekit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class CollectionsExTestCase {

	@Test
	public void test() {
		List<String> a=new ArrayList<>(Arrays.asList(new String[]{"aaa","bbb","ccc","aaa"}));
		int n=CollectionsEx.distinct(a);
		assertEquals(a.size(), 3);
		assertEquals(a.get(0), "aaa");
		assertEquals(a.get(1), "bbb");
		assertEquals(a.get(2), "ccc");
		assertEquals(n, 1);
	}
	
	@Test
	public void test2(){
		List<String> a=new ArrayList<>(Arrays.asList(new String[]{"-configuration","Release","-quiet","CONFIGURATION_BUILD_DIR=/opt/teamcat_space/t1_Develop-iOS-ESports/build1"}));
		int a1 =CollectionsEx.indexOfIgnoreCase(a, "CONFIGURATION_BUILD_DIR");
		 a1=2;
	}

}
